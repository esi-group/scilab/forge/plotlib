function out=_compute_data_bounds(ax,_entity,logflags)
    
  global _SHADED_ENTITIES
  
  if argn(2)<3
    logflags=ax.log_flags;
  end
  
  out=list();
  
  old_db=ax.user_data.data_bounds;
  
  if size(ax.children)==0
    db=[0 1 0 1 0 1 0 1]';
  else
    db=%inf*[1 -1 1 -1 1 -1 1 -1]';
    for i=1:size(ax.children,1)
      h=ax.children(i);
      while h.type=="Compound" & type(h.user_data)==1
        h=h.children;
      end   
      for j=1:size(h,1)
        if typeof(h(j).user_data)=="leafData"
           hdb=h(j).user_data.data_bounds;
           db=[min(hdb(1),db(1))
               max(hdb(2),db(2))
               min(hdb(3),db(3))
               max(hdb(4),db(4))
               min(hdb(5),db(5))
               max(hdb(6),db(6))
               min(hdb(7),db(7))
               max(hdb(8),db(8))];
        end
     end
    end
    for i=1:2:7
      _min=db(i);_max=db(i+1);
      if (_max-_min)<%eps
        if abs(_min)<%eps
            _min=-1; _max=1;
        elseif _min<0
            _max=0;
            _min=2*_min;
        else
            _min=0;
            _max=_max*2;
        end  
      end
      db(i:i+1)=[_min;_max];
    end
  end

  ax.user_data.data_bounds=db;
  
  if (ax.view=='2d' & or([ax.user_data.XLimMode ax.user_data.YLimMode]=='auto')) ...
    | (ax.view=='3d' & or([ax.user_data.XLimMode ax.user_data.YLimMode ax.user_data.ZLimMode]=='auto'))
    
    IMD=ax.parent.immediate_drawing;
    ax.parent.immediate_drawing='off';
    // gory trick to get the pretty values of limits on all axes
      
    old_angles=ax.rotation_angles;
    old_log_flags=ax.log_flags;
    ax.log_flags='nnn';
    ax.rotation_angles=[60,230];
    ax.data_bounds=db(1:6);
    ax.log_flags=logflags;
    ax.tight_limits='off';
    xlim=ax.x_ticks.locations([1 $]);
    ylim=ax.y_ticks.locations([1 $]);
    zlim=ax.z_ticks.locations([1 $]);
    ax.user_data.pretty_data_bounds=[xlim' ylim' zlim' db(7:8)'];
    ax.rotation_angles=old_angles;
    ax.tight_limits='on';
    ax.log_flags=old_log_flags;
    
    if ax.user_data.XLimMode=="auto"
      ax.user_data.XLim(:)=ax.user_data.pretty_data_bounds(1:2);
    end
    if ax.user_data.YLimMode=="auto"
      ax.user_data.YLim(:)=ax.user_data.pretty_data_bounds(3:4);
    end
    if ax.user_data.ZLimMode=="auto"
       ax.user_data.ZLim(:)=ax.user_data.pretty_data_bounds(5:6);
    end
    if ax.user_data.CLimMode=="auto"
      ax.user_data.CLim(:)=db(7:8);
    end
    
    if ax.isoview=='on' & ax.view=="2d"
      [ax.user_data.XLim,ax.user_data.XLimMode,ax.user_data.YLim,ax.user_data.YLimMode]=_ratio_one_lims(ax);
    end
    
    ax.data_bounds=[ax.user_data.XLim(:) ax.user_data.YLim(:) ax.user_data.ZLim(:)];
    
    ax.parent.immediate_drawing=IMD
  end

  if ax.user_data.CLimMode=="auto" & or(old_db(7:8)~=db(7:8))
     _update_shaded_plots(ax);
  elseif or(_entity.user_data.typeOfPlot==[_SHADED_ENTITIES,'image']) 
     _update_single_shaded_pl(_entity.handle);
  end   

          
endfunction
