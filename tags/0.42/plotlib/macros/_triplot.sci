function _triplot(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0   
  load(PLOTLIB+'tridem.dat')
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';
  _triplot(nodes,xy(1,:),xy(2,:),'edgecolor',[0 0 1]);
  _axis equal
  h.immediate_drawing=IMD;
else
   _mainPlot('triplot',varargin);
end

endfunction /////
