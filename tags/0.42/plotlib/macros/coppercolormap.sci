function c = coppercolormap(m)

// copper color map.

c = min(1,graycolormap(m)*diag([1.2500 0.8 0.5]));

endfunction








