function [_out,__errorText]=_checkTypeAndDimensions(_arg,_i,_string,_type,_dims)
  _out=%t;
  __errorText=[];
   if and(type(_arg)~=_type)
    _out=%f;
    _typeString(13)="function";
    _typeString(1)="matrix";    
    __errorText=sprintf("%s : argument %d must be a ",_string,_i);
    for i=1:length(_type)
      if i>1 
        __errorText=__errorText+" or ";
      end
      __errorText=__errorText+_typeString(_type(i));
    end
    if argn(1)==2
      return
    end
    _error(__errorText);
   end
  
  if argn(2)==4
    return
  end
 
    _size=size(_arg);
    
    _check=%f;
    for i=1:size(_dims,1)
      if and(_dims(i,:)==-1)
        _result=%t;
      elseif or(_dims(i,:)==-1)
        _result=or(size(_arg)==_dims(i,:));
      else
        _result=and(size(_arg)==_dims(i,:));
      end
      _check=_check | _result;
    end
    
    if ~_check
       _out=%f;
      __errorText=sprintf("%s : argument %d must be a matrix of size ",_string,_i);
      for i=1:size(_dims,1)
        if i>1 
          __errorText=__errorText+" or ";
        end
        __errorText=__errorText+sprintf("%s x %s",strsubst(string(_dims(i,:)),'-1','n') );
      end
      if argn(1)==2
        return
      end
      _error(__errorText);
    end

endfunction

function [_parsed_arguments,_properties]=_parse(typeOfPlot,argList)

  global reqMatrixArgs

  _start=0;
  _parsed_arguments=list();
  
  _found_a_string=0;
  for i=1:length(argList) // finding the number of matrix/function args
     _type(i)=type(argList(i));
     if _type(i)==10
        _found_a_string=1;
         break
     else
         _checkTypeAndDimensions(argList(i),i,typeOfPlot,[1,13]);
     end
  end
  
  _matrix_args=i-_found_a_string;
  
  if and(_matrix_args~=reqMatrixArgs(typeOfPlot)) // Checking the total number of arguments
    __errorText=string(reqMatrixArgs(typeOfPlot)(1));
    for i=2:length(reqMatrixArgs(typeOfPlot))-1
        __errorText=__errorText+", "+string(reqMatrixArgs(typeOfPlot)(i))
    end
    if length(reqMatrixArgs(typeOfPlot))>1
        __errorText=__errorText+" or "+string(reqMatrixArgs(typeOfPlot)($));
    end
    _error(sprintf("%s : expecting %s matrix arguments",typeOfPlot,__errorText));
  end

  if or(typeOfPlot==["trimesh";"tripcolor";"trisurf";"trisurfl"]); // Connectivity matrix
    _checkTypeAndDimensions(argList(1),1,typeOfPlot,1,[3,-1]);
    _parsed_arguments($+1)=argList(1);
   _start=1;
  end

  _with_opt_arg=%f;
   if size(reqMatrixArgs(typeOfPlot),2)==2
    _with_opt_arg=or(_matrix_args==reqMatrixArgs(typeOfPlot)(:,2)); // Optionnal argument is used
  end
 
  if or(typeOfPlot==["mesh";"pcolor";"surf";"surfl";"quiver";"quiver3"])
    if or(_matrix_args==reqMatrixArgs(typeOfPlot)(:,1)) // no (_X,_Y) pair
      _checkTypeAndDimensions(argList(1),1,typeOfPlot,1);
      [ny,nx]=size(argList(1));
      [_X,_Y]=meshgrid(1:nx,1:ny);
    elseif _type(1:2)==1
      if isvector(argList(1)) & isvector(argList(2)) //(_X,_Y) pair but vector type
         [_X,_Y]=meshgrid(argList(1),argList(2));
         _start=2;
      end      
    end
  end
    
  if ~exists('_X') 
     _checkTypeAndDimensions(argList(_start+1),1,typeOfPlot,1);
     _X=argList(_start+1);
     _checkTypeAndDimensions(argList(_start+2),2,typeOfPlot,1,size(_X)); // All other cases : parse (_X,_Y)
     _Y=argList(_start+2);
     _start=_start+2;
  end
  
  _parsed_arguments=lstcat(_parsed_arguments,_X,_Y);
  
   for i=_start+1:_matrix_args
       
      if _with_opt_arg & or(typeOfPlot==["quiver";"quiver3"]) & i==_matrix_args // looking for field factor
  
         _checkTypeAndDimensions(_argList(i),i,typeOfPlot,1,[1,1]);
      
      elseif or(typeOfPlot==["fill";"fill3"]) & i==_matrix_args // vector or matrix for the color
        
         _checkTypeAndDimensions(_argList(i),i,typeOfPlot,1,[1,size(_X,2);size(_X)]);

      elseif or(typeOfPlot==["tripcolor";"trisurf"]) & i==_matrix_args // vector or matrix for the color
        
         _checkTypeAndDimensions(_argList(i),i,typeOfPlot,1,[1,size(argList(1),2);size(_X)]);
      
      elseif _type(i)==1 // matrix with size of _X
         
         _checkTypeAndDimensions(_argList(i),i,typeOfPlot,1,size(_X));

      elseif _type(i)==13 // Scilab function

        [nArgOut1,vectInput]=check3dFun(typeOfPlot,_argList(i),_X,_Y);
        if ~vectInput | (nArgOut1~=1 & nArgOut1~=3)
            _error(sprintf('%s : function must return 1 or 3 output arguments and accept matrix inputs',typeOfPlot));
        end
      
      end 
      
      _parsed_arguments($+1)=_arg;
  
  end

  _properties=list(argList(_matrix_args+1:$));

endfunction
