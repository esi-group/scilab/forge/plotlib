function hdl=_imagesc(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  load(PLOTLIB+'mandrill.dat');
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';
  hdl=_image(X);
  _colormap gray
  _axis equal
  _axis tight
  h.immediate_drawing=IMD;
else
   hdl=_mainPlot('image',varargin,'CDataMapping','scaled');
end

endfunction /////
