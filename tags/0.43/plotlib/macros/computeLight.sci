function [l]=computeLight(n,vect)

l=vect'*matrix(n,[3,-1]);
l=l.*(l>0);
endfunction
