function out=%pltlibH_get(h,varargin)
  
  global leafPropertiesNames axesPropertiesNames figurePropertiesNames
  
  if is_handle_valid(h.handle)
    user_data=get(h.handle,'user_data');
  else
    error('get : handle is no more valid');
  end
  
  select typeof(h.handle.user_data)
  case 'figureData'
    propertiesNames=figurePropertiesNames;
    typ='Figure';
  case 'axesData'
    propertiesNames=axesPropertiesNames;
    typ='Axes'
  case 'leafData'
    propertiesNames=leafPropertiesNames;
    typ='Leaf';
  else
    propertiesNames=[];
    typ=h.handle.type;
  end
  
  if length(varargin)==0
    prop=propertiesNames(1)(2:$);
  else
    prop=varargin(1);
  end
  
  for i=1:size(prop,'*')
    aprop=convstr(prop(i));
    if or(convstr(aprop)==propertiesNames(1))
      ppty=propertiesNames(aprop)(1);
      out(ppty)=user_data(ppty);
    else
      try
        out(aprop)=[]; // usefull trick...
        value=get(h.handle,aprop);
        if typeof(value)=='handle' // children or parent case
          value=mlist(['pltlibH','handle'],value);
        end
        out(aprop)=value;
      catch
        _error(sprintf('get : %s is an unknown %s property name',prop,typ));  
      end  
    end
  end
  if size(fieldnames(out),'*')==1
    out=out(fieldnames(out));
  end

endfunction

