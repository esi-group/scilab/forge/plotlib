function hdl=mesh(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  f=gcf();
  IMD=f.immediate_drawing;
  f.immediate_drawing='off';
  [u,v]=meshgrid(linspace(0,%pi,30),linspace(0,%pi,30));
  h1=mesh(sin(v).*cos(u),sin(v).*sin(u),cos(v),'edgecolor','blue');
  hold on
  h2=mesh(sin(v).*cos(u),1-sin(v).*sin(u),cos(v),'edgecolor','red');
  hold off
  axis equal
  axis([-1 1 -1 2 -1 1])
  hdl=[h1 h2];
  f.immediate_drawing=IMD;
else
   hdl=_mainPlot('mesh',varargin);
end

endfunction /////
