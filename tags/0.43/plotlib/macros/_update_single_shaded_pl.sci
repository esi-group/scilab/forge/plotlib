function _update_single_shaded_pl(h)  
  C=computeColor(h.user_data);
  if or(h.user_data.typeOfPlot==_SHADED_ENTITIES)
    h.data.color=C(h.user_data.vertex_indices);
  else
    h.data=C($:-1:1,:);
  end
endfunction
