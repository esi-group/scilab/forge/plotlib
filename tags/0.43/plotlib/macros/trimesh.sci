function hdl=trimesh(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0
  load(PLOTLIB+'dinosaure.dat')
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';
  hdl=trimesh(nodes,x,y,z,'edgecolor',[0 0 1]);
  hidden off
  axis equal
  h.immediate_drawing=IMD;
else
   hdl=_mainPlot('trimesh',varargin);
end

endfunction /////
