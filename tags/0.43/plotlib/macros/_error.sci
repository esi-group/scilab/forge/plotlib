function _error(_str)
  global IMD
  if get(get('current_figure'),"immediate_drawing")=="on"
     IMD="on";
  end
  set(get('current_figure'),'immediate_drawing',IMD);
  error(_str);
endfunction
