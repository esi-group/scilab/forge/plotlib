function out = parseHideMode(typeOfPlot,value,pptystring,ppty)

//
// Parsing function for the 'hidden' property 
//

select type(value)
case 10 //  a string
   if or(value==["on";"yes"])
      out=list("FaceColor","default");   
   elseif or(value==["off";"no"])
      out=list("FaceColor","none");
   else
      _error(sprintf('%s : unknown hide mode ''%s''',typeOfPlot,value));
   end
else
   _error(sprintf('%s : missing hide mode ',typeOfPlot));
end

endfunction
