// tests

h=figure(0);
drawlater;
clf;
colormap(jetcolormap(256));

cmds=['pcolor','tripcolor','surf','surfl',...
'trisurf','trisurfl','mesh','trimesh',...
'triplot','quiver','quiver3','plot3',...
'fill','fill3','loglog','plotyy'];

for i=1:16
  ax=subplot(4,4,i);
  execstr(cmds(i));
  title(ax,cmds(i));
end  

colormap jet
drawnow
messagebox("Double-click on an Axes to give it position [0 0 1 1]")
