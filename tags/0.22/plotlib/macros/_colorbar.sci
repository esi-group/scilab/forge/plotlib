function _colorbar(varargin)   

global DONE IMD

if length(varargin)==0
  varargin(0)='on'
end
varargin(0)="colorbar";
[cb]=parseColorBar('_colorbar',varargin)

win=safeInit();
if DONE==%f;
    win.immediate_drawing=IMD; 
else
    DONE=%f;
end

IMD=win.immediate_drawing;
win.immediate_drawing="off";

ax=gca();
win=gcf();
changeVP(win,ax,cb);
if cb~="off"
  processColorBar(cb,win,ax);
end

win.immediate_drawing=IMD;
DONE=%t;

endfunction
