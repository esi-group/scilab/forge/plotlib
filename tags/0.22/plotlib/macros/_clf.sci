function _clf(varargin)

global defaultFigureUserData DONE IMD


if type(defaultFigureUserData)==1 // cannot compare a tlist with [] !
	graphinit();
end


if winsid()==[]
   win=_fig();
end



[lhs,rhs]=argn(0);

if rhs==1
   win=varargin(1);
else
   win=gcf();
end

if DONE==%f
 win.immediate_drawing=IMD; 
end

IMD=win.immediate_drawing;
win.immediate_drawing="off";

clearWindow(win,'clf')

win.immediate_drawing=IMD;

DONE=%t;

endfunction
