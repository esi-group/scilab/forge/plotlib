function _shg(varargin)

win=safeInit();
if length(varargin)==0
  show_window(win);
else
  show_window(varargin(1));
end
endfunction
