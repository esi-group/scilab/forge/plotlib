function changeVP(win,ax,cb)    

fact=win.user_data.margin;
vp=ax.user_data.viewport;

x=vp(1);y=vp(2);
w=vp(3);h=vp(4);
dy=h*fact;
dx=w*fact;
dxl=dx*1.1;
dyl=dy*1.3;

X=x+dxl;
Y=y+dy;
LX=w-dx-dxl;
LY=h-dy-dyl;
cbech=[];

DIV=20;

if cb=="left"
	X=x+2*dxl+w/DIV;
	LX=w-2*dxl-dx-w/DIV;
	LY=h-dy-dyl;
	cbech=[x+dxl y+dy w/DIV LY];	
elseif cb=="right"
	X=x+dxl
	Y=y+dy;
	LX=w-2*dxl-dx-w/DIV;
	LY=h-dy-dyl;
	cbech=[x+LX+2*dxl y+dy w/DIV LY];	
elseif cb=="top"
	X=x+dxl;
	Y=y+2*dy+h/DIV;
	LY=h-2*dy-h/DIV-dyl;
	cbech=[X y+dy LX h/DIV];
elseif cb=="bot"
	X=x+dxl;
	Y=y+dy;
	LY=h-2*dy-h/DIV-dyl;
	cbech=[X y+dy+dyl+LY LX h/DIV];
end
ax.axes_bounds=[X Y LX LY];

if cb~='off'
    if ax.user_data.colorbarPosition=='off'
      ax.user_data.colorbarHandle=newaxes();
    elseif ax.user_data.colorbarPosition ~= cb
      delete(ax.user_data.colorbarHandle.children);      
    end
    colorbarHandle=ax.user_data.colorbarHandle;
    colorbarHandle.axes_bounds=cbech;
    colorbarHandle.visible="on";
    ax.user_data.colorbarPosition=cb;
elseif ax.user_data.colorbarPosition~='off'
    execstr("delete(ax.user_data.colorbarHandle)","errcatch");
    ax.user_data.colorbarPosition='off';
end
sca(ax);
endfunction
