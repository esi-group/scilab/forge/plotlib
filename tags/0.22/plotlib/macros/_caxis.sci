function [varargout]=_caxis(varargin) 

[lhs,rhs]=argn(0);

win=safeInit();
ax=gca();

if rhs==1
   if type(varargin(1))==1
      if length(varargin(1))==2
         ax.user_data.caxis=varargin(1)(:)';
		 ax.user_data.caxisMode='manual';
      else
         error('caxis : vector must have the form [cmin cmax]');
      end
	  
   elseif type(varargin(1))==10
      select varargin(1)
      case 'auto'
         ax.user_data.caxisMode='auto';
	     ax.user_data.caxis=[]
      case 'manual'
         if ax.user_data.caxis~=[]
	     	ax.user_data.caxisMode='manual';
	 	else
	    	error('caxis : cannot set caxis to manual mode, caxis is empty');
	 	end
      else
         error('caxis : unknown caxis mode');
      end
   end
elseif rhs>0
   error('caxis : too many input arguments');      
end
varargout(1)=ax.user_data.caxis;

endfunction
