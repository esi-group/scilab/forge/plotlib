function hdl=_trimesh(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0
   load(PLOTLIB+'dinosaure.dat')
   hdl=_trimesh(nodes,x,y,z,'hidden','off','axis','equal')
else
   hdl=mainPlot3d('trimesh',varargin);
end

endfunction /////
