function _add_event_handler(win,_function_name)

global %plotlib_global

if type(win)==1
  h=scf(win);
else
  h=win;
  win=h.figure_id;
end

h.user_data.eventHandlers($+1)=_function_name;
%plotlib_global.eventHandlers(win+1)=h.user_data.eventHandlers;

endfunction
