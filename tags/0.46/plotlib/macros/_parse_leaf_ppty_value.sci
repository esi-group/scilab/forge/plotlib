function [_leaf_ppty_value_list,_next]=_parse_leaf_ppty_value(typeOfPlot,argList,_start)
  
global %plotlib_global
    
_leaf_ppty_value_list=list();

_next=_start;

while _next <= length(argList)

  if type(argList(_next))~=10
    break;
  else    
    [_ppty_value_list,fail]=getColorNumber(typeOfPlot,argList(_next)); 
    _leaf_ppty_value_list=lstcat(_leaf_ppty_value_list,_ppty_value_list);
  end    

  if ~fail
      _next=_next+1;
  elseif _next == length(argList)
      _error(sprintf('%s : unknown property name ""%s"" or missing value',typeOfPlot,argList(_next)));
  else    
      pptystring=convstr(argList(_next));
      value=argList(_next+1);
     
      if or(pptystring==%plotlib_global.leafPropertiesNames(1))
      
            ppty=%plotlib_global.leafPropertiesNames(pptystring)(1);              
            parseFunction=%plotlib_global.leafPropertiesNames(pptystring)(2);
            cmd=sprintf("%s(typeOfPlot,value,""%s"",""%s"")",parseFunction,pptystring,ppty);
            _ppty_value_list=evstr(cmd);
            _leaf_ppty_value_list=lstcat(_leaf_ppty_value_list,_ppty_value_list);
      else
           _error(sprintf('%s : ""%s"" is an unknown property name',typeOfPlot,pptystring));
      end // select convstr(_pair(1))

     _next=_next+2;

  end

end  
  
endfunction
