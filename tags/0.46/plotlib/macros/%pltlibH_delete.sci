function %pltlibH_delete(h)
  
  if  typeof(h.handle.user_data)=='leafData'  
    _ax=get(h.handle,'parent');
    while get(_ax,'type')~='Axes'
      _ax=get(_ax,'parent');
    end
    delete(h.handle); 
    _compute_data_bounds(_ax,h);
  else
    delete(h.handle);
  end
  
endfunction
