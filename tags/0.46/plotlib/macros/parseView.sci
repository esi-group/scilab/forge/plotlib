function out=parseView(typeOfPlot,value,pptystring,ppty);

if type(value)==1
 p=value;
 if size(p,1)==1
    p=p';
 end
 [li,co]=size(p);
 if  li==1
  if value==2
     az=0;el=90;
  elseif value==3
     az=-37.5;el=30;
  else
    _error(sprintf('%s : view specification must be a 1, 2 or 3-vector',typeOfPlot))
  end
 elseif li==2
    az=p(1,:);
    el=p(2,:);
 elseif li==3
    ez=zeros(1,co);
    al=zeros(1,co);
    for k=1:co
      el(k)=asin(p(3,k)/norm(p(:,k)))/%pi*180;
      az(k)=atan(p(2,k),p(1,k))/%pi*180+90;
    end
 else
    _error(sprintf('%s : view specification must be a 1, 2 or 3-vector',typeOfPlot))
 end
else
 _error(sprintf('%s : view specification must be a vector',typeOfPlot))
end

out=list(ppty,[az,el]);

endfunction // end of parseView
