function focus(ax_to_focus)
  
  if argn(2)==0
    ax_to_focus=get('current_axes');
  end
  win=ax_to_focus(1).parent;
  win.immediate_drawing="off";
  ax=win.children;
  others_visible='off';  
  if ax_to_focus(1).user_data.OuterPosition==[0 0 1 1] // focus out
    others_visible='on';
  end
  for i=1:length(ax)
    if or(ax(i)==ax_to_focus)
      if ax(i).user_data.OuterPosition==[0 0 1 1] // focus out
        _update_axes(ax(i),list('OuterPosition',ax(i).user_data.previousPosition));
        _message='';
      else // focus in
        ax(i).user_data.previousPosition=ax(i).user_data.OuterPosition;
        _update_axes(ax(i),list('OuterPosition',[0 0 1 1]));
        _message='Plotlib message : double-click within axes to restore initial position';     
      end
    else
      ax(i).visible=others_visible;
    end
  end

  win.immediate_drawing="on";
  win.info_message=_message;
endfunction
