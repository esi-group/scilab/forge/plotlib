function hdl=imagesc(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  load(plotlibpath()+'mandrill.dat');
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';
  hdl=image(X);
  colormap gray
  axis equal
  axis tight
  h.immediate_drawing=IMD;
else
   hdl=_mainPlot('image',varargin,'CDataMapping','scaled');
end

endfunction /////
