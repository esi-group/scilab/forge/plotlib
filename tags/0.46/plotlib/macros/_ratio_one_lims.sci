function [XLim,XLimMode,YLim,YLimMode]=_ratio_one_lims(ax)

  XLimMode=ax.user_data.XLimMode;
  YLimMode=ax.user_data.YLimMode;
  XLim=ax.user_data.XLim;
  YLim=ax.user_data.YLim;
    
  if XLimMode=='manual' & YLimMode=='manual' 
    return
  end
        
  if XLimMode=='auto'  
    dx=ax.user_data.data_bounds(2)-ax.user_data.data_bounds(1);
    XLim=ax.user_data.data_bounds(1:2);
  else
    dx=ax.user_data.XLim(2)-ax.user_data.XLim(1);
    XLim=ax.user_data.XLim;
  end  
  if YLimMode=='auto'  
    YLim=ax.user_data.data_bounds(3:4);
    dy=ax.user_data.data_bounds(4)-ax.user_data.data_bounds(3);
  else      
    dy=ax.user_data.YLim(2)-ax.user_data.YLim(1);
    YLim=ax.user_data.YLim;
  end      
  if dx==0
   data_ratio=%inf;
  else
   data_ratio=dy/dx;
  end
  
  axes_screen_ratio=ax.parent.axes_size(2)*ax.axes_bounds(4)/ax.parent.axes_size(1)/ax.axes_bounds(3);

  if (data_ratio>=axes_screen_ratio &  XLimMode=='auto') | (XLimMode=='auto' & YLimMode=='manual')
    w=dy/axes_screen_ratio
    XLim=mean(ax.user_data.data_bounds(1:2))+[-w/2 w/2];
  else
    _h=dx*axes_screen_ratio
    YLim=mean(ax.user_data.data_bounds(3:4))+[-_h/2 _h/2];
  end
endfunction