function out = parseLabel(typeOfPlot, value,pptystring,ppty)

//
// Parsing label for the 'xlabel,ylabel,zlabel,...' properties 
//

select type(value)
case 10 //  a string
case 1
  if value~=[]
   _error(sprintf('%s : missing %s ',typeOfPlot, ppty));
  end
  value="";
end

out=list(ppty,value);


endfunction
