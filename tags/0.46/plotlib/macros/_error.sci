function _error(_str)
  set(get('current_figure'),'immediate_drawing','on');
  error(_str);
endfunction
