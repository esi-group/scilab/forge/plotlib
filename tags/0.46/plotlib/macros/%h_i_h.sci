function cible=%h_i_h(varargin)
 
  source=varargin($-1) //the handle to be inserted
  cible=varargin($)  //the destination handle
  ind=varargin(1)  //the path
  temp=cible(ind(1)) //ind(1) is "user_data" so temp is the user_data value
  if size(ind)==2 then
    if type(ind($))==15 then //last index is a matrix index: user_data(i,j)
      last=ind($)
      temp(last(:))=source
    else //user_data(i) or user_data.xxx
      temp(ind(2))=source
    end
  else
    if type(ind($))==15 then //last index is a matrix index: user_data.xxx....(i,j)
      temp2=temp(list(ind(2:$-1)))
      last=ind($)
      temp2(last(:))=source
      temp(list(ind(2:$-1)))=temp2
    else // user_data.xxx....(i) or user_data.xxx...yyy
      temp(list(ind(2:$)))=source
    end
  end
  cible(ind(1))=temp  //put the user data_value back into the graphic data structure
endfunction


