function hold(varargin) 


i=1;
ax=get('current_axes');
cmd=[];
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
       ax=varargin(i).handle;
     else
      _error("axis : handle should be an axes handle")
     end
     i=i+1;
   elseif or(varargin(i)==['on','off'])
     cmd=varargin(i);
     i=i+1;
   else
     _error('hold : unknown hold state (must be ''on'' or ''off'')')
  end
end

if cmd==[]
  if ax.user_data.nextPlot=='add'
    ax.user_data.nextPlot='erase'
    disp('Current plot released')
  else
    ax.user_data.nextPlot='add';
    disp('Current plot held')
  end
elseif cmd=='on'
  ax.user_data.nextPlot='add';
elseif cmd=='off'
  ax.user_data.nextPlot='erase';
end  

endfunction
