function out=parseCDataMapping(typeOfPlot,value,pptystring,ppty)

select type(value)
case 10 //  a string
  if and(value~=['scaled','direct'])
       _error(sprintf('%s : % value must be ''scaled'' or ''direct''',typeOfPlot,ppty));
    end
else
   _error(sprintf('%s : missing %s spec',typeOfPlot,ppty));
end

out=list(ppty,value);

endfunction
