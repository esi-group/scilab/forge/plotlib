function plotlib_handler(_win,_x,_y,_ibut)

	global eventHandlers 

    _h=scf(_win);
    IMD=_h.immediate_drawing;
    _h.immediate_drawing='off';
    _h.event_handler_enable='off';
    _h.immediate_drawing=IMD;
    _flag=%f;
	if _ibut~=-1000
    
        for _handler=_h.user_data.eventHandlers($:-1:1)
	        [_flag,_err]=evstr(_handler+"(_win,_x,_y,_ibut,_flag)");
	        if _err
                _h.event_handler_enable='on';
                _h.immediate_drawing=IMD;
		        error("Plotlib: error in handler function "+_handler+" when processing event "+string(_ibut));
	        end
        end
   else
        if ~execstr("eventHandlers(_win+1)","errcatch")
            for _handler=eventHandlers(_win+1)
	            [_flag,_err]=evstr(_handler+"(_win,_x,_y,_ibut,_flag)");
	            if _err
                   _h.event_handler_enable='on';
                   _h.immediate_drawing=IMD;
		            error("Plotlib: error in handler function "+_handler+" when processing event "+string(_ibut));
	            end
            end
        end
   end
   
   _h.immediate_drawing='off';
   _h.event_handler_enable='on';
   _h.immediate_drawing=IMD;
 
endfunction
