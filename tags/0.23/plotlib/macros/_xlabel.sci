function _xlabel(labelString)
    win=safeInit();
    ax=gca();
    ax.x_label.text=labelString;
    ax.x_label.font_foreground=ax.foreground;
endfunction
