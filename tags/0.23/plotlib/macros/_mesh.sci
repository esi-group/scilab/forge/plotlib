function hdl=_mesh(varargin)

[lhs,rhs]=argn(0);

if rhs==0

   x=-1:0.1:1;
   y=x;
   deff('z=f(x,y)','z=cos(%pi*x.*y)');
   hdl=_mesh(x,y,f);
else
   hdl=mainPlot3d('mesh',varargin);
end

endfunction /////
