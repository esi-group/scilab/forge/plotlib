function out=plotlib_subplot_handler(win,x,y,ibut,in)
  out=in;
  if ibut==10
    h=scf(win);
    _axes=h.children;
    i_matching=0;
    numberOfAxes=size(_axes,1);
    for i=1:numberOfAxes;
     xl=_axes(i).axes_bounds(1)*h.figure_size(1);
     w=_axes(i).axes_bounds(3)*h.figure_size(1);
     yu=_axes(i).axes_bounds(2)*h.figure_size(2);
     _h=_axes(i).axes_bounds(4)*h.figure_size(2);
     if (x-xl >= 0) & (x-xl <=w) & (y-yu >=0) & (y-yu<=_h) & _axes(i).visible=="on" & type(_axes(i).user_data)~=1
        i_matching=i;
     end
    end
    if i_matching
         focus(_axes(i_matching));
		 out=%t;
     end
  end
endfunction
