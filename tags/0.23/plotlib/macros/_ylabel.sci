function _ylabel(labelString)
    win=safeInit();
    ax=gca();
    ax.y_label.text=labelString;
    ax.y_label.font_foreground=ax.foreground;
endfunction
