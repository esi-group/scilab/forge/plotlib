function process2D3DPrelim(win,ax,Xscale,Yscale,Zscale,...
						axisVect,axisTightX,axisTightY,...
						axisRatio,axisStyle,colorBar,ticksx,ticksy,ticksz,az,el)

if ax.user_data.nextPlot=='add'
   return
end


ax.font_style=6;
//ax.font_size=setFontSize(ax.axes_bounds);

ax.axes_visible='on';
ax.font_color=ax.foreground;
ax.box='on';


select axisStyle
   case 'off'
	  ax.axes_visible='off';
      ax.box='off';
      ax.filled="off";
   case 'box'
      ax.box='on';
	  ax.axes_visible='off';
   case 'normal'
      ax.x_location='bottom';
      ax.y_location='left';
   case 'right'
      ax.x_location='bottom';
      ax.y_location='right';
   case 'center'
      ax.x_location='middle';
      ax.y_location='middle';
   case 'origin'
      ax.x_location='origin';
      ax.y_location='origin';
end

if axisRatio=='equal'
    if ax.view=="3d"
        ax.cube_scaling='off';
    end
    ax.isoview="on";
elseif axisRatio=='auto'
    if ax.view=="3d"
        ax.cube_scaling='off';
    end
    ax.isoview="off";
elseif  ax.view=="3d"
    ax.cube_scaling='on';
end

if axisTightX | axisTightY
    ax.tight_limits="on";
end

if axisVect~=[]
    ax.data_bounds=axisVect;
	ax.auto_scale="off";
end

if ticksx=='none'
    xt=ax.x_ticks;
    xt.labels=[];
    xt.locations=[];
    ax.x_ticks=xt;
end
if ticksy=='none'
    yt=ax.y_ticks;
    yt.labels=[];
    yt.locations=[];
    ax.y_ticks=yt;
end
if ticksz=='none'
    zt=ax.z_ticks;
    zt.labels=[];
    zt.locations=[];
    ax.z_ticks=zt;
end

modeScale='';

if Xscale=='log'
   modeScale=modeScale+'l';
else
   modeScale=modeScale+'n';
end

if Yscale=='log'
   modeScale=modeScale+'l';
else
   modeScale=modeScale+'n';
end

if Zscale=='log'
   modeScale=modeScale+'l';
else
   modeScale=modeScale+'n';
end

ax.log_flags=modeScale;

if ax.view=="3d" & az~=[] & el~=[]
  ax.rotation_angles=[el,az];
end


changeVP(win,ax,colorBar)

endfunction
