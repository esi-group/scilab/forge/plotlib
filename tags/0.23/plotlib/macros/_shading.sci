function _shading(varargin)

global DONE IMD

if length(varargin)==0
    return
end

varargin(0)="_shading";
parseShading("_shading",varargin);
shadingType=varargin(2);


win=safeInit();
if DONE==%f;
    win.immediate_drawing=IMD; 
else
    DONE=%f;
end

IMD=win.immediate_drawing;
win.immediate_drawing="off";

h=get(gca(),'children');

for i=1:length(h);
  if h(i).type=="Fac3d" & size(h.data.x)==size(h.data.color)
    if shadingType=="interp"
      h(i).color_flag=3;
      h(i).color_mode=-1;
    elseif shadingType=="faceted"
     h(i).color_flag=4;
     h(i).color_mode=2;
    elseif shadingType=="flat"
     h(i).color_flag=4;
     h(i).color_mode=-1;    
    end
  end
end

win.immediate_drawing=IMD;

endfunction // end of _shading
