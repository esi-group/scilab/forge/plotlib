function hdl=mainPlot(typeOfPlot,argList)

global defaultFigureUserData DONE IMD

hdl=[];

minX=%inf;
maxX=-%inf;
minY=%inf;
maxY=-%inf;

currentColor=1;
argNumber=1;
marker=[];
numberOfLegends=0;
matOfLegends=[];
typeOfLegend=[];

axisRatio='auto';
axisTightX=%F;
axisTightY=%F;
axisStyle='normal';
axisVect=[];

ticksx=[];
ticksy=[];

foreground=[];
background=[];

Xlabel=[];
Ylabel=[];
Title=[];

gridFlag=%f;
gridColor=[];

win=safeInit();

if DONE==%f;
 win.immediate_drawing=IMD; 
else
  DONE=%f;
end
IMD=win.immediate_drawing;
win.immediate_drawing="off";

currentAxes=gca(); // get the handle of current axes

if currentAxes.user_data.nextPlot=='erase'
	_cla;
end

currentAxes.visible="on";

while length(argList)
    
   sizes=[1 1];
   
   if type(argList(1))==15 // If the first element is itself a list 

      tempList=argList(1); // expand this list in the main argument list
      argList(1)=null();
      for i=length(tempList):-1:1
          argList(0)=tempList(i);
      end
   end
   
   if type(argList(1))==1 // If the first argument is a matrix
	
	 if argList(1)==[]
	 	return;
	end
	

      if length(argList)==1 // If only one argument

         if or(size(argList(1))==1)  // If this is a vector

            argList(0)=1:length(argList(1)); // insert an abcsissa vector of same length,

         else                                  // if this is a matrix,

            argList(0)=1:size(argList(1),1); // insert an abcsissa vector with 

         end                                   // length = number of lines of 1st argument
         argNumber=argNumber-1;

      else

         if type(argList(2))==10 
         // If the second argument is a string
            if or(size(argList(1))==1) // same treatment as above
               argList(0)=1:length(argList(1));
            else
               argList(0)=1:size(argList(1),1);
            end
            argNumber=argNumber-1;
         end

      end

      if (type(argList(2))==1 | type(argList(2))==13)  // If the second argument is a matrix or a function

         [X,Y]=_checkXYPair(typeOfPlot,argList(1),argList(2)); // verify (x,y) consistency
         
         argList(1)=null(); // Deletion of the two top arguments
         argList(1)=null(); // in the argument list

         argNumber=argNumber+2;

         _colors=0; lineStyle=1; marker=[];

         if length(argList) // If there is a next argument

            if (type(argList(1))==10) // If this argument is a string

               [_colors,marker,sizes,lineStyle,fail]=getColorNumber(win,argList(1)); 

               if ~fail // the string seems to be a maker/color combination
                 
                 argList(1)=null(); // Delete the top argument in the argument list
                 argNumber=argNumber+1;
               else
                  _colors=0; lineStyle=1; marker=[];
               end
            end

         end

         if size(X,2)==1 // If the abscissa is a vector             
			  hdl=[hdl;addPlot(typeOfPlot,X(:,ones(1,size(Y,2))),Y,[],_colors,marker,sizes,lineStyle,currentAxes)];
         else // Abscissa and ordina are both matrices
                hdl=[hdl;addPlot(typeOfPlot,X,Y,[],_colors,marker,sizes,lineStyle,currentAxes)];
         end

      end

   elseif (type(argList(1))==10) // If this argument is a string

      select argList(1) // Try to identify a property name

      case 'axis'
         [axisStyle,axisRatio,axisVect,axisTightX,axisTightY] = ...
	 parseAxis(typeOfPlot,argList,axisStyle,axisRatio,axisVect,axisTightX,axisTightY);
	       
	 argList(1)=null(); argList(1)=null();

      case 'ticksX'
         [ticksx] = parseTicks(typeOfPlot,argList);
	       
	 argList(1)=null(); argList(1)=null();

      case 'ticksY'
         [ticksy] = parseTicks(typeOfPlot,argList);
	       
	 argList(1)=null(); argList(1)=null();

      case 'background'
         background = parseColor(typeOfPlot,'background',argList);
         argList(1)=null(); argList(1)=null();

      case 'foreground'
         foreground = parseColor(typeOfPlot,'foreground',argList);
         argList(1)=null(); argList(1)=null();
         
      case 'edgecolor'
         edgecolor = parseColor(typeOfPlot,'edgecolor',argList);
         argList(1)=null(); argList(1)=null();

      case 'facecolor'
         facecolor = parseColor(typeOfPlot,'facecolor',argList);
         argList(1)=null(); argList(1)=null();
 
      case 'Xscale'
         Xscale=parseScale(typeOfPlot,'Xscale',argList);
         argList(1)=null(); argList(1)=null();

      case 'Yscale'
         Yscale=parseScale(typeOfPlot,'Yscale',argList);
         argList(1)=null(); argList(1)=null();

      case 'legend'

         argList(1)=null();
 	 [matOfLegends,nbProc,typeOfLegend]=parseLegend(typeOfPlot,argList);         
         if nbProc==0
            error('plot : missing string(s) for legend');
	 end
         for k=1:nbProc; argList(1)=null(); end;

      case 'grid'
      
         [gridFlag,gridColor]=parseGrid(typeOfPlot,argList);
	 argList(1)=null(); argList(1)=null();
	 
      case 'xlabel'
         Xlabel = parseLabel(typeOfPlot,'xlabel',argList);
	 argList(1)=null(); argList(1)=null();
	 
      case 'ylabel'
         Ylabel = parseLabel(typeOfPlot,'ylabel',argList);
	 argList(1)=null(); argList(1)=null();
	 
      case 'title'
      
         Title = parseLabel(typeOfPlot,'title',argList);
	 argList(1)=null(); argList(1)=null();
      
      else
         error(sprintf('plot : %s is an unknown property name',argList(1)));
      end // select argList(1)

   else
      str=sprintf('plot : argument %d has not the expected type',argNumber);
      error(str);

   end // if type(argList(1))

end // while length(argList)

// Common 2D stuff

process2D3DPrelim(win,currentAxes,Xscale,Yscale,[],axisVect,axisTightX,axisTightY,axisRatio,axisStyle,'off',ticksx,ticksy,[])

if background~=[]
	currentAxes.background=findColorIndex(background);
end

if foreground~=[]
	currentAxes.foreground=findColorIndex(foreground);
end

if gridFlag
  if length(gridColor)==3
    gridColor=findColorIndex(gridColor);
  end
  currentAxes.grid=[gridColor gridColor gridColor];
end

// Now process the legends (if applicable)

if matOfLegends ~= []
   processLegend(currentAxes,matOfLegends,typeOfLegend);
end

if Xlabel~=[]
   _xlabel(Xlabel);
end
if Ylabel~=[]
   _ylabel(Ylabel);
end

if Title~=[]
    _title(Title)
end

if currentAxes.user_data.hidden=="yes"
  currentAxes.visible="off";
  execstr('currentAxes.user_data.colorbarHandle.visible=''off''','errcatch');
  execstr('currentAxes.user_data.legendHandle.visible=''off''','errcatch');
end

win.immediate_drawing=IMD;
DONE=%t;


endfunction
