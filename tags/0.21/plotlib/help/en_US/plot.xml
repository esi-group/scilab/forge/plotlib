<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * Ajouter ici d'�ventuels commentaires sur le fichier XML
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="plot" xml:lang="en">
  <info>
    <pubdate>$LastChangedDate: 26-05-2009 $</pubdate>
  </info>
  <refnamediv>
    <refname>plot</refname>
    <refpurpose>linear 2D plot</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>hdl = plot(X,Y)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>X</term>
        <listitem>
          <para>1 by n or m by n matrix</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Y</term>
        <listitem>
          <para>1 by n or m by n matrix</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>hdl</term>
        <listitem>
          <para>Handle of the created Polyline entity</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>plot(X,Y) plots vector Y versus vector X. If X or Y is a matrix,
    then the vector is plotted versus the rows or columns of the matrix,
    whichever line up.</para>
    <para>plot(Y) plots the columns of Y versus their index. In all uses of
    plot, the imaginary part is ignored.</para>
    <para>Various line thickness/style, plot symbols or colors may be obtained
    with plot(X,Y,S) where S is a character string made from one element from
    any or all the following 3 colunms</para>
    <table>
      <title/>
      <tgroup cols="3">
        <tbody>
          <row>
            <entry>y : yellow</entry>
            <entry>. point</entry>
            <entry>- : solid</entry>
          </row>
          <row>
            <entry>m : magenta</entry>
            <entry>o circle</entry>
            <entry>-- : dashed</entry>
          </row>
          <row>
            <entry>c : cyan</entry>
            <entry>+ plus</entry>
            <entry>-. : dash-dot</entry>
          </row>
          <row>
            <entry>r : red</entry>
            <entry>* star</entry>
            <entry>2 : double thickness</entry>
          </row>
          <row>
            <entry>g : green</entry>
            <entry>d diamond</entry>
            <entry>3 : triple thickness</entry>
          </row>
          <row>
            <entry>b : blue</entry>
            <entry>f filled diamond</entry>
            <entry>... up to 9 times</entry>
          </row>
          <row>
            <entry>w : white</entry>
            <entry>t trefoil</entry>
            <entry/>
          </row>
          <row>
            <entry>b : black</entry>
            <entry>^ triangle up</entry>
            <entry/>
          </row>
          <row>
            <entry/>
            <entry>v triangle down</entry>
            <entry/>
          </row>
        </tbody>
      </tgroup>
    </table>
    <para>For example, plot(X,Y,'-c+') plots a cyan solid line with a plus at
    each data point; plot(X,Y,'bd') plots blue diamond at each data point but
    does not draw any line.</para>
    <para>plot(X1,Y1,S1,X2,Y2,S2,X3,Y3,S3,...) combines the plots defined by
    the (X,Y,S) triples, where the X's and Y's are vectors or matrices and the
    S's are strings. For example, plot(X,Y,'y-',X,Y,'go') plots the data
    twice, with a solid yellow line interpolating green circles at the data
    points.</para>
    <para>The plot command, if no color is specified, makes automatic use of
    the colors.</para>
    <para>When f is a function then plot(x,f) plots the parametric or non
    parametric curve defined by f :</para>
    <itemizedlist>
      <listitem>
        <para>If f has the syntax [y]=f(x) then plot(x,f) plots the
        non-parametric y=f(x),</para>
      </listitem>
      <listitem>
        <para>If f has the syntax [x,y]=f(t) then plot(t,f) plots the
        parametric curve [x(t),y(t)], example :</para>
      </listitem>
    </itemizedlist>
    <programlisting>deff('y=f(x)','y=x.^2');
x=-1:0.1:1;
plot(x,f);
    
deff('[x,y]=circle(t)','x=cos(t);y=sin(t)');
deff('[x,y]=lissajous(t)','x=cos(t);y=sin(2*t)');
t=linspace(0,2*%pi,63);
plot(t,circle,'-g^r',t,lissajous,'2','axis','equal');</programlisting>
    <para>The X,Y pairs, or X,Y,S triples, can be followed by parameter/value
    pairs to specify additional properties of the plot.</para>
  </refsection>
  <refsection>
    <title>Screenshot</title>
    <screenshot>
      <mediaobject>
        <imageobject>
          <imagedata fileref="plot.png"/>
        </imageobject>
      </mediaobject>
    </screenshot>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="fig">fig</link>
      </member>
      <member>
        <link linkend="clf">clf</link>
      </member>
      <member>
        <link linkend="axis">axis</link>
      </member>
      <member>
        <link linkend="xlabel">xlabel</link>
      </member>
      <member>
        <link linkend="ylabel">ylabel</link>
      </member>
      <member>
        <link linkend="title">title</link>
      </member>
      <member>
        <link linkend="semilogx">semilogx</link>
      </member>
      <member>
        <link linkend="semilogy">semilogy</link>
      </member>
      <member>
        <link linkend="loglog">loglog</link>
      </member>
      <member>
        <link linkend="hold">hold</link>
      </member>
      <member>
        <link linkend="subplot">subplot</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>St�phane Mottelet</member>
    </simplelist>
  </refsection>
</refentry>
