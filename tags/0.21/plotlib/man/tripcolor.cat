 TRIPCOLOR  2D Triangular pseudocolor plot.
 
    tripcolor(TRI,X,Y,C) displays the triangles defined in the 3-by-M
    face matrix TRI as pseudocolor patches.  A row of TRI contains indexes 
    into the X,Y vertex vectors to define a single triangular face.
    The color is defined by the vector C.

    tripcolor(...,'param','value','param','value'...) allows additional
    param/value pairs to be used. See pcolor for the possible 
    param/value pairs.
    
    example :
       
    load(PLOTLIB+'tridem.dat')
    fig('background',[0 0 1])
    colormap hot
    subplot(2,2,1)
    tripcolor(nodes,xy(1,:),xy(2,:),P(:,1),'colorbar','on', ...
              'axis','equal','edgecolor',[1 1 0])
    subplot(2,2,2)
    tripcolor(nodes,xy(1,:),xy(2,:),P(:,1),'shading','flat', ...
              'colorbar','on','axis','equal')
    subplot(2,2,3)
    tripcolor(nodes,xy(1,:),xy(2,:),P(:,1),'shading','interp', ...
              'colorbar','on','axis','equal')      
	      
	      
    See also pcolor, surf.
                                          
