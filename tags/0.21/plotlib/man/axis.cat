 AXIS  Sets ot gets the axis limits of a 2D or 3D graph


    --- To get the axis limits, use the function axis :
    
    AX=axis()
    
    The structure of the output is AX=[XMIN XMAX YMIN YMAX] or
    AX=[XMIN XMAX YMIN YMAX ZMIN ZMAX]
    

    --- To set the axis limits, use the property 'axis' in a plot
    command e.g.
    
    plot(X,Y,'axis',value) 
    
    or 
    
    mesh(X,Y,Z,'axis','value')
    
    value can be :
    
    - a 4-vector [XMIN XMAX YMIN YMAX] 
    or 6-vector [XMIN XMAX YMIN YMAX ZMIN ZMAX]
    
    then the plot limits are fixed to the given limits.
    
    - a string : the allowed strings are
    
    Common 2D/3D values
    
    * 'equal'	(isometric scale)
	* 'off'		(no axis is drawn)
	* 'box'		(a box enclosing the plot without ticks)

    2D Specific values

	* 'tightX' 	(the graph fits the X-axis)
	* 'tightY' 	(the graph fits the Y-axis)
    * 'tight'  	(the graph fits the axis)
    * 'left'	(the y axis is displayed on the left)
	* 'right'	(the y axis is displayed on the right)
	* 'center'	(the x and y axis cross at the center of the plot box)
	* 'origin'	(the x and y axis cross at the (0,0) point)

    3D specific values
    
	* 'vis3d'	(isometric/non expanded scale)
	* 'trihedral'	(3 small x/y/z axis are drawn)

    Here are some examples :

    plot(cos(t),sin(t),'axis','equal')    
    plot(cos(t),sin(t),'axis',[0 1 0 1])
