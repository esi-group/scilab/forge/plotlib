mode(-1)
if isdef('plotlib')
   clear
   predef(0);
   clear plotlib
   predef all
end
pathB=get_absolute_file_path('builder.sce')
getf(pathB+'macros/plotlibver.sci');
disp('Building plotlib '+plotlibver()+' in ' +pathB+'macros')
genlib('plotlib',pathB+'macros',%t)
if exists('xmltojar')
  xmltojar(pathB+"help/en_US","Matlab-like plotting library",'en_US');
end
clear pathB

