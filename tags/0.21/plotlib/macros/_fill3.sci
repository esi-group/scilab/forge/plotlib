function hdl=_fill3(varargin)

[lhs,rhs]=argn(0);

if rhs==0
else
   hdl=mainPlot3d('fill3',varargin);
end

endfunction /////
