function processColorBar(typ,win,ax)

cax=ax.user_data.caxis;
if cax==[]
  cax=[0 1];
  if win.user_data.RGBcolormaptable==[]
     win.color_map=win.user_data.colormap;
     win.user_data.RGBcolormaptable=win.user_data.colormap;
  end
end
table=win.user_data.RGBcolormaptable;
sca(ax.user_data.colorbarHandle);
ax.user_data.colorbarHandle.visible='on';

if typ=='left' | typ=='right'

    xfpolys([0 1 1 0]',[cax(1) cax(1) cax(2) cax(2)]',[table(1) table(1) table($) table($)]');
	ax.user_data.colorbarHandle.data_bounds=[0 cax(1);1 cax(2)];
    ax.user_data.colorbarHandle.axes_visible(2)='on';

elseif typ=='top' | typ=='bot'

    xfpolys([cax(1) cax(2) cax(2) cax(1)]',[0 0 1 1]',[table(1) table($) table($) table(1)]');
	ax.user_data.colorbarHandle.data_bounds=[cax(1) 0;cax(2) 1];
    ax.user_data.colorbarHandle.axes_visible(1)='on';

end

ax.user_data.colorbarHandle.tight_limits='on';
ax.user_data.colorbarHandle.box='on';
ax.user_data.colorbarHandle.foreground=ax.foreground;
ax.user_data.colorbarHandle.font_color=ax.font_color;

sca(ax);

endfunction
