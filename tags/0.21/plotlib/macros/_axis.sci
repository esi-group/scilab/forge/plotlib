function vect=_axis(varargin)

global DONE IMD

win=safeInit();
ax=gca();

[lhs,rhs]=argn(0);

if rhs>0
  varargin(0)="axis";
  [axisStyle,axisRatio,axisVect,axisTightX,axisTightY]=parseAxis('axis',varargin,'normal','auto',[],%F,%F);

  if DONE==%f;
     win.immediate_drawing=IMD; 
  else
     DONE=%f;
  end
  win.immediate_drawing="off"; 
  
  select varargin(2)
  case 'off'
      ax.axes_visible='off';
      ax.box='off';
  case 'box'
      ax.box='on';
	ax.axes_visible='off';
  case 'left'
      ax.x_location='bottom';
      ax.y_location='left';
  case 'right'
      ax.x_location='bottom';
      ax.y_location='right';
  case 'center'
      ax.x_location='middle';
      ax.y_location='middle';
  case 'origin'
      ax.x_location='middle';
      ax.y_location='middle';
  case 'equal'
    if ax.view=="3d"
        ax.cube_scaling='off';
    end
    ax.isoview="on";
  case "auto"
      if ax.view=="3d"
        ax.cube_scaling='off';
      end
      ax.isoview="off";
  case "normal"
      if ax.view=="3d"
        ax.cube_scaling='off';
      end
      ax.isoview="off";
  case "tight"
    ax.tight_limits="on";
  case "tightX"
    ax.tight_limits="on";
  case "tightY"
    ax.tight_limits="on";
  end

  if axisVect~=[]
    ax.data_bounds=axisVect;
	ax.auto_scale="off";
  end

  win.immediate_drawing=IMD;
  DONE=%t;
  vect=[];
  
else
  
  vect=ax.data_bounds;
  vect=vect(:);

end

endfunction
