function win=safeInit()

global defaultFigureUserData DONE IMD

if and(driver()~=['GIF','PPM','Pos']) & (winsid()==[])
  _fig()
end

win=gcf(); // get the handle of current graphic window
ax=gca();

if type(win.user_data)==1
    if type(defaultFigureUserData)==1 // cannot compare a tlist with [] !
	    graphinit();
    end    
    win.user_data=defaultFigureUserData;
    IMD=win.immediate_drawing;
    DONE=%t;
end

if type(ax.user_data)==1
    clearWindow(win,'clf');
end
endfunction
