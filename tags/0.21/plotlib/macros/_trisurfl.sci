function hdl=_trisurfl(varargin)

[lhs,rhs]=argn(0);

if rhs==0
   load(PLOTLIB+'dinosaure.dat')
   hdl=_trisurfl(nodes,x,y,z,'axis','equal','facecolor','interp')
else
   hdl=mainPlot3d('trisurfl',varargin);
end

endfunction /////
