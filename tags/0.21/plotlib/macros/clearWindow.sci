function clearWindow(win,typeOfPlot,cmap)

    global defaultAxisUserData

	[lhs,rhs]=argn(0);
		
	if rhs==2 & typeOfPlot~="clf" & win.user_data.RGBcolormaptable==[]
	    win.color_map=win.user_data.colormap;
	end

	win.background=findColorIndex(win.user_data.frameColor);
    
    if typeOfPlot~="fig"
      if (win.pixmap=='off') | or(driver()==['GIF','PPM','Pos'])
        scilab_clf(win);
      else
        scilab_clf(win);
      end
    end

    ax=gca(); // current axis, likely
    ax.background=findColorIndex(win.user_data.background);
    ax.foreground=findColorIndex(win.user_data.foreground);
    ax.user_data=defaultAxisUserData;
    ax.axes_bounds=win.user_data.defaultViewport;
    ax.user_data.currentColor=1;
    ax.visible="off";
endfunction
