function _hold(varargin) 

[lhs,rhs]=argn(0);

win=safeInit();
ax=gca();

if rhs==1
  
  if varargin(1)=='on'
    ax.user_data.nextPlot='add';
  elseif varargin(1)=='off'
    ax.user_data.nextPlot='erase';
  else
    error('hold : unknown hold state (must be ''on'' or ''off'')')
  end
elseif rhs==0
  if  ax.user_data.nextPlot=='add'
    ax.user_data.nextPlot='erase'
    disp('Current plot released')
  else
    ax.user_data.nextPlot='add';
    disp('Current plot held')
  end
end

endfunction
