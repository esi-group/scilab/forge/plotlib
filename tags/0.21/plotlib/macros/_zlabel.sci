function _zlabel(labelString)
    win=safeInit();
    ax=gca();
    ax.z_label.text=labelString;
    ax.z_label.font_foreground=ax.foreground;
endfunction
