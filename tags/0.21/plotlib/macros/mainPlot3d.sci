function hdl=mainPlot3d(typeOfPlot,argList)

global defaultFigureUserData DONE IMD MAXCOL

// typeOfPlot can take the following values :
//
// plot3
// (tri)mesh
// (tri)surf
// (tri)surfl
// (tri)pcolor
// triplot
// fill

hdl=[];
hidden=%T;
numberOfVertices=4;     // default type of polygons
lightVect=[1;1;1];      // default position of light source (at infinity)
azimuth=[];             // default azimuth
elevation=[];         // default elevation
facecolor=[];           // default facecolor for 'mesh'
BackFaceColor="none";
BackFaceCulling="no";
edgecolor=[];           // default edgecolor for 'mesh'
markersize=1;
shadingType='faceted';
dejavu=0;
surfaceIsParam=%F;
colorBar='off'
Xlabel=' ';
Ylabel=' ';
Zlabel=' ';
Title=[];
typeOfLegend=[];
matOfLegends=[];
ticksx=[];
ticksy=[];
ticksz=[];
field_factor=1;

gridFlag=%f;
gridColor=[];

minX=%inf;
maxX=-%inf;
minY=%inf;
maxY=-%inf;
minZ=%inf;
maxZ=-%inf;

listOfTriples=list();
currentColor=1;
argNumber=1;
marker=[];

axisRatio='auto';
axisTightX=%F;
axisTightY=%F;
axisStyle='normal';
axisVect=[];

foreground=[];
background=[];

win=safeInit();
if DONE==%f;
    win.immediate_drawing=IMD; 
else
    DONE=%f;
end

IMD=win.immediate_drawing;
win.immediate_drawing="off";

currentAxes=gca(); // get the hdl of current axes

previous_view=currentAxes.view
if currentAxes.user_data.nextPlot=='erase'
    _cla;
end

currentAxes.visible='on';

while length(argList)

    sizes=[1 0];

    if type(argList(1))==15 // If the first element is itself a list 

        tempList=argList(1); // expand this list in the main argument list
        argList(1)=null();
        for i=length(tempList):-1:1
            argList(0)=tempList(i);
        end
    end

    if type(argList(1))==1 // If the first argument is a matrix

        // then the second argument must be also a matrix.
        // The 3rd argument can be a matrix or a macro (function)

        if length(argList)==1 & typeOfPlot~='plot3'
            s=size(argList(1));
            argList(0)=1:s(1); // insert an abscissa vector of size n,
            argList(0)=1:s(2); // insert an ordina vector of size m,
            argNumber=argNumber-2;
        end

        if length(argList)>=2
            if type(argList(2)) ~= 1
                s=size(argList(1));
                argList(0)=1:s(1); // insert an abscissa vector of size n,
                argList(0)=1:s(2); // insert an ordina vector of size m,
                argNumber=argNumber-2;
            else
                if length(argList)>=3
                    if type(argList(3)) ~= 1 & type(argList(3)) ~= 13 // likely "quiver" style
                        s=size(argList(1));
                        argList(0)=1:s(1); // insert an abscissa vector of size n,
                        argList(0)=1:s(2); // insert an ordina vector of size m,
                        argNumber=argNumber-2;
                    end
                else
                    s=size(argList(1));
                    argList(0)=1:s(1); // insert an abscissa vector of size n,
                    argList(0)=1:s(2); // insert an ordina vector of size m,
                    argNumber=argNumber-2;
                end
            end
        end

        if length(argList)>=3

            if type(argList(2))==1 & (type(argList(3))==1 | type(argList(3))==13)

                // We verify (X,Y,Z,C) consistency, and eventually generate the 'real' Z
                // if Z was a function in the input argument list (mesh or surf case)

                if typeOfPlot=='trimesh' | ...
                    typeOfPlot=='trisurf' | ...
                    typeOfPlot=='trisurfl' | ...
                    typeOfPlot=='tripcolor'


                    if length(argList)>=5
                        if (type(argList(5))==1 | type(argList(5))==13) // a matrix or a function for the color
                            [X,Y,Z,triang]=_checkXYZQuadruple(typeOfPlot,argList(1),argList(2),...
                            argList(3),argList(4),argList(5));
                            argList(1)=null();argList(1)=null();argList(1)=null();argList(1)=null();
                            argList(1)=null();
                            argNumber=argNumber+5;
                        else
                            [X,Y,Z,triang]=_checkXYZQuadruple(typeOfPlot,argList(1),argList(2),...
                            argList(3),argList(4));
                            argList(1)=null();argList(1)=null();argList(1)=null();argList(1)=null();
                            argNumber=argNumber+4;
                        end

                    else
                        [X,Y,Z,triang]=_checkXYZQuadruple(typeOfPlot,argList(1),argList(2),...
                        argList(3),argList(4));
                        argList(1)=null();argList(1)=null();argList(1)=null();argList(1)=null();
                        argNumber=argNumber+4;
                    end		  

                elseif ((typeOfPlot=='surf') | (typeOfPlot=='fill3') | (typeOfPlot=='pcolor') | (typeOfPlot=="quiver")) & length(argList)>=4 

                    if (type(argList(4))==1 | type(argList(4))==13) // a matrix or a function for the color
                        [X,Y,Z,surfaceIsParam]=_checkXYZQuadruple(typeOfPlot,argList(1),argList(2),...
                        argList(3),argList(4));
                        argList(1)=null();argList(1)=null();argList(1)=null();argList(1)=null();
                        argNumber=argNumber+4;
                    else
                        [X,Y,Z,surfaceIsParam]=_checkXYZQuadruple(typeOfPlot,argList(1),argList(2),argList(3));
                        argList(1)=null();argList(1)=null();argList(1)=null();
                        argNumber=argNumber+3;
                    end

                elseif typeOfPlot=='triplot'

                    [X,Y,Z,triang]=_checkXYZQuadruple(typeOfPlot,argList(1),argList(2),argList(3));
                    argList(1)=null();argList(1)=null();argList(1)=null();
                    argNumber=argNumber+3;

                else // e.g. "surf,surfl, fill, ..."

                    [X,Y,Z,surfaceIsParam]=_checkXYZQuadruple(typeOfPlot,argList(1),argList(2),argList(3));
                    argList(1)=null();argList(1)=null();argList(1)=null();
                    argNumber=argNumber+3;

                end

                _colors=0; lineStyle=1; marker=[];
				
				if length(argList) & typeOfPlot=='quiver' // If there is a next argument
					if type(argList(1))==1 // if this is a field factor, likely
						field_factor=argList(1);
						argList(1)=null();
					end
				end

                if length(argList) & ( typeOfPlot=='plot3' | typeOfPlot=='quiver' ) // If there is a next argument
                    if type(argList(1))==10 // If this argument is a string
                        // and this is a plot3() or quiver() call

                        [_colors,marker,sizes,lineStyle,fail]=getColorNumber(win,argList(1)); 
                        if ~fail // the string seems to be a maker/color combination
                            argList(1)=null(); // Delete the top argument in the argument list
                            argNumber=argNumber+1;
                        else
                            _colors=0; lineStyle=1; marker=[];
                        end  
                    end
                end

                if typeOfPlot=="plot3" | typeOfPlot=="quiver"
                    hdl=[hdl;addPlot(typeOfPlot,X,Y,Z,_colors,marker,sizes,lineStyle,currentAxes,field_factor)];
                elseif dejavu==0
                    dejavu=1;
                else
                    error(sprintf('%s : too many input arguments',typeOfPlot));
                end

            else
                error(sprintf('%s : arguments %d,%d and %d must be matrices',...
                typeOfPlot,argNumber,argNumber,argNumber+1));			
            end

        else
            error(sprintf('%s : not enough input arguments',typeOfPlot))      
        end

    elseif (type(argList(1))==10) // If this argument is a string

        select convstr(argList(1)) // Try to identify a property name

        case 'colorbar'
            colorBar=parseColorBar(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'light'
            lightVect=parseLight(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'backfacelighting'
            BackFaceColor=parseBackFaceLighting(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'backfacecolor'
            BackFaceColor=parseColor(typeOfPlot,'backfacecolor',argList);
            argList(1)=null(); argList(1)=null();
        case 'backfaceculling'
            BackFaceCulling=parseBackFaceCulling(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'view'
            [azimuth,elevation]=parseView(typeOfPlot,argList,azimuth,elevation);
            argList(1)=null(); argList(1)=null();
        case 'facecolor'
            facecolor = parseColor(typeOfPlot,'facecolor',argList);
            argList(1)=null(); argList(1)=null();
        case 'edgecolor'
            edgecolor = parseColor(typeOfPlot,'edgecolor',argList);
            argList(1)=null(); argList(1)=null();
        case 'shading'
            [facecolor,edgecolor]=parseShading(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'axis'
            [axisStyle,axisRatio,axisVect,axisTightX,axisTightY] = ...
            parseAxis(typeOfPlot,argList,axisStyle,axisRatio,axisVect,axisTightX,axisTightY);
            argList(1)=null(); argList(1)=null();
        case 'ticksx'
            [ticksx] = parseTicks(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'ticksy'
            [ticksx] = parseTicks(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'background'
            background = parseColor(typeOfPlot,'background',argList);
            argList(1)=null(); argList(1)=null();
        case 'foreground'
            foreground = parseColor(typeOfPlot,'foreground',argList);
            argList(1)=null(); argList(1)=null();
        case 'xlabel'
            Xlabel = parseLabel(typeOfPlot,'xlabel',argList);
            argList(1)=null(); argList(1)=null();
        case 'ylabel'
            Ylabel = parseLabel(typeOfPlot,'ylabel',argList);
            argList(1)=null(); argList(1)=null();
        case 'title'
            Title = parseLabel(typeOfPlot,'title',argList);
            argList(1)=null(); argList(1)=null();
        case 'zlabel'
            Zlabel = parseLabel(typeOfPlot,'zlabel',argList);
            argList(1)=null(); argList(1)=null();
        case 'hidden'
            hidden = parseHideMode(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'grid'
            [gridFlag,gridColor]=parseGrid(typeOfPlot,argList);
            argList(1)=null(); argList(1)=null();
        case 'legend'
            argList(1)=null();
            [matOfLegends,nbProc,typeOfLegend]=parseLegend(typeOfPlot,argList);         
            if nbProc==0
                error('bar : missing string(s) for legend');
            end
            for k=1:nbProc; argList(1)=null(); end;

        else
            error(sprintf('%s : %s is an unknown property name',typeOfPlot,argList(1)));
        end // select argList(1)

    else
        str=sprintf('%s : argument %d has not the expected type',typeOfPlot,argNumber);
        error(str);

    end // if type(argList(1))
end // while length(argList)

// Common 2D/3D stuff


if typeOfPlot=='mesh' | typeOfPlot=='trimesh' | typeOfPlot=='triplot'
else
    if facecolor==[]
        facecolor="flat";
    end
end

// Now we process the list of plots to do

if typeOfPlot=='plot3'

    process2D3DPrelim(win,currentAxes,'linear','linear','linear',...
    axisVect,axisTightX,axisTightY,axisRatio,axisStyle,colorBar,...
    ticksx,ticksy,ticksz,azimuth,elevation)

elseif typeOfPlot=="quiver"

    process2D3DPrelim(win,currentAxes,'linear','linear','linear',...
    axisVect,axisTightX,axisTightY,axisRatio,axisStyle,colorBar,...
    ticksx,ticksy,ticksz,azimuth,elevation)
    
    if currentAxes.user_data.nextPlot=="erase"
      currentAxes.view="2d";
    end

else // Color plots

    colormaptable=win.user_data.RGBcolormaptable;
    if colormaptable==[]
        RGBmap=win.user_data.RGBcolormap;
        n1=size(win.user_data.colormap,1);
        n2=size(RGBmap,1);
        win.user_data.RGBcolormaptable=n1+1:n1+n2;
        win.color_map=[win.user_data.colormap;RGBmap];
        win.background=findColorIndex(win.user_data.frameColor');
        colormaptable=win.user_data.RGBcolormaptable;
    end

    nc=length(colormaptable);

    if type(Z)==1 // if the surface/patch is defined by numerical data

        if typeOfPlot=="fill"
        elseif typeOfPlot=='trisurfl' | typeOfPlot=='trisurf' | ...
               typeOfPlot=='tripcolor' | typeOfPlot=='trimesh' |...
               typeOfPlot=='triplot'

            ntri=size(triang,2);
            triang=triang($:-1:1,:);
            nnodes=length(X);

            if typeOfPlot=='tripcolor' & length(Z)~=length(X) // when color is given for triangles, not nodes
                Z=Z(:)';
                if facecolor=='interp' // Averaging of value for each node
                    C=zeros(nnodes,1);
                    nV=zeros(nnodes,1);
                    t=zeros(3,1);
                    M=[-1 -1 0;1 0 1;0 1 -1];
                    for i=1:ntri // for each triangle, nodal values are weighted
                        nodes=triang(:,i); // by the measure of the associated angular sector
                        D=[X(nodes);Y(nodes)]*M;
                        D=D*diag(1../sqrt(sum(D.^2,'r')));
                        s=D'*D;
                        t=acos(s(2:3,1));t(3)=%pi-[1 1]*t;
                        nV(nodes)=nV(nodes)+t;
                        C(nodes)=C(nodes)+Z(i)*t;
                    end
                    Z=C./(nV+%eps);
                    clear C;
                    Z=matrix(Z(triang),3,ntri);
                end
            elseif typeOfPlot~='triplot'
                Z=matrix(Z(triang),3,ntri);
            end

            X=matrix(X(triang),3,ntri); 
            Y=matrix(Y(triang),3,ntri);

            if typeOfPlot=='trisurfl' // Computation of normals and illumination

                N=-[(Y(2,:)-Y(1,:)).*(Z(3,:)-Z(1,:))-(Z(2,:)-Z(1,:)).*(Y(3,:)-Y(1,:));
                (Z(2,:)-Z(1,:)).*(X(3,:)-X(1,:))-(X(2,:)-X(1,:)).*(Z(3,:)-Z(1,:));
                (X(2,:)-X(1,:)).*(Y(3,:)-Y(1,:))-(Y(2,:)-Y(1,:)).*(X(3,:)-X(1,:))];

                N=N*sparse([1:ntri;1:ntri]',1../(%eps+sqrt(sum(N.^2,'r'))));

                if facecolor=='interp' // Averaging of illumination for each node

                    L=computeLight(N,lightVect);
                    clear N;
                    C=zeros(nnodes,1);
                    nV=zeros(nnodes,1);
                    for i=1:ntri
                        nodes=triang(:,i);
                        C(nodes)=C(nodes)+L(i);
                        nV(nodes)=nV(nodes)+1;
                    end
                    clear L;
                    C=C./(nV+%eps);
                    Z=Z+%i*matrix(C(triang),3,ntri);
                    clear nV C
                else
                    C=computeLight(N,lightVect);
                    clear N;
                end

            end

        elseif ~surfaceIsParam    // fill3, surf, surfl, mesh, pcolor, non-parametric case

            nx=length(X);
            ny=length(Y);
            if typeOfPlot=='surfl'
                [zx,zy]=nonParametricDiffData(X,Y,Z');
                Z=Z+%i*matrix(computeLight(nonParametricNormals(zx,zy),lightVect),ny,nx)';
            end
            [X,Y,Z]=genfac3d(X,Y,Z);
            
        else // fill3, surf, surfl, mesh, pcolor, parametric case

            [nv,nu]=size(X)          
            if typeOfPlot=='surfl'
                [xu,yu,zu,xv,yv,zv]=parametricDiffData(X,Y,Z);
                Z=Z+%i*matrix(computeLight(parametricNormals(xu,yu,zu,...
                xv,yv,zv),lightVect),nv,nu);
            end

            // Now convert X,Y and Z to polygons	

            if typeOfPlot=='fill3'
            else
                [X,Y,Z]=generate3dPolygons(X,Y,Z,numberOfVertices,surfaceIsParam);                
            end    
        end

    elseif type(Z)==13 // if the surface is defined by a function

        Zf=Z; clear Z;

        if typeOfPlot=='surfl'
            truc=lighten(Zf,lightVect);
            [X,Y,Z]=eval3dPolygons(X,Y,truc,numberOfVertices,surfaceIsParam);
        else
            [X,Y,Z]=eval3dPolygons(X,Y,Zf,numberOfVertices,surfaceIsParam);
        end

    end

    if typeOfPlot=='surf' | ...
       typeOfPlot=='pcolor' | ...
       typeOfPlot=='fill' | ...
       typeOfPlot=='tripcolor' | ...
       typeOfPlot=='surfl' | ...
       typeOfPlot=='trisurf' | ...
       typeOfPlot=='trisurfl' | ...
       typeOfPlot=="triplot" | ...
       typeOfPlot=="fill3" | ...
       typeOfPlot=='mesh' | ...
       typeOfPlot=='trimesh'

        if typeOfPlot=="triplot" | typeOfPlot=='mesh' | typeOfPlot=='trimesh'
            C=zeros(1,size(X,2));
            if hidden
                if facecolor==[]
                    C=C+currentAxes.background;
                else
                    C=C+findColorIndex(facecolor);
                end
            end
			if edgecolor==[]
				edgecolor=win.color_map(currentAxes.user_data.currentColor,:);
			end
			
        else

            if isreal(Z)
                C=Z;
            else
                C=imag(Z); Z=real(Z);   
            end
//            if facecolor=='flat'
//                C=sum(C,'r')/size(C,1);
//            end

            minC=min(C);maxC=max(C);

            if minC==maxC
                if minC==0
                    minC=-1; maxC=1;
                elseif minC<0
                    maxC=0;
                    minC=2*minC;
                else
                    minC=0;
                    maxC=maxC*2;
                end
            end
			
            if currentAxes.user_data.caxisMode=='auto' // if the color axis is automatic
                C=round((C-minC)/(maxC-minC+%eps)*(nc-1))+1;
                currentAxes.user_data.caxis=[minC maxC];
            else // the color axis has been held
                minCmaxC=currentAxes.user_data.caxis;
                minC=minCmaxC(1);
                maxC=minCmaxC(2);
                C=round((C-minC)/(maxC-minC+%eps)*(nc-1))+1;
                C(C<1)=1; C(C>nc)=nc; // treshold the colors
            end

            if facecolor=='interp' |  facecolor=='flat'
                C=matrix(colormaptable(C),size(Z));
            elseif facecolor=="none"
                C=colormaptable(C);
            else // facecolor is a colorspec
                C=findColorIndex(facecolor);
                C=C(ones(1,size(X,2)));
            end
        end

       if typeOfPlot=="pcolor" | typeOfPlot=="tripcolor" | typeOfPlot=="fill" | typeOfPlot=="triplot"
	    Z=0*X;
	   end

	    if axisVect==[] & currentAxes.children==[]
			currentAxes.data_bounds=[min(X) min(Y) min(Z);max(X) max(Y) max(Z)];
        end

        previous_view=currentAxes.view
        if typeOfPlot=="pcolor" | typeOfPlot=="tripcolor" | typeOfPlot=="fill" | typeOfPlot=="triplot" // 3D style but 2D plots ...
			plot3d1(X,Y,list(Z,C));
			if previous_view=="2d" | currentAxes.user_data.nextPlot=="erase"
				currentAxes.view="2d";
			end
		else
			previous_angles=currentAxes.rotation_angles;
			plot3d1(X,Y,list(Z,C));
           if previous_view=="3d" & elevation == [] & azimuth == []
               elevation=previous_angles(1);
               azimuth=previous_angles(2);
           end
		end
        h=gce();

        process2D3DPrelim(win,currentAxes,'linear','linear','linear',...
        axisVect,axisTightX,axisTightY,axisRatio,axisStyle,colorBar,...
        ticksx,ticksy,ticksz,azimuth,elevation);	

        if BackFaceCulling=="yes"
            hc=0;
        else
            if BackFaceColor=="auto";
                hc=mean(colormaptable);		
            elseif BackFaceColor=="none";
                hc=-1;
            else 
                hc=findColorIndex(BackFaceColor);
            end            
        end

        h.hiddencolor=hc;
        
        if edgecolor=='none'
            h.color_mode=-1;
        elseif edgecolor~=[]
            h.foreground=findColorIndex(edgecolor);
      end
      
      if facecolor=="flat"
          h.color_flag=4;
      end
			
    else
        str=sprintf('mainPlot3d : %s is an unknown type of plot ',typeOfPlot);
        error(str);
    end
    hdl=h;
end

// Now process the remaning stuff (if applicable)

if background~=[]
    currentAxes.background=findColorIndex(background);
end

if foreground~=[]
    currentAxes.foreground=findColorIndex(foreground);
end

if colorBar ~= 'off'
    processColorBar(colorBar,win,currentAxes);
end


if matOfLegends ~= []
    processLegend(currentAxes,matOfLegends,typeOfLegend);
end

if Xlabel~=[]
    _xlabel(Xlabel);
end

if Ylabel~=[]
    _ylabel(Ylabel);
end

if Zlabel~=[]
    _zlabel(Zlabel);
end

if Title~=[]
    _title(Title)
end

win.immediate_drawing=IMD;
DONE=%t;



endfunction 
