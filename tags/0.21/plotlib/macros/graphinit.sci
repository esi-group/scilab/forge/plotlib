function graphinit(varargin)

global defaultFigureUserData defaultAxisUserData MAXCOL

//
// Initialisation of graphic state. Must be called once at startup
//

[lhs,rhs]=argn(0);

// Initialisation of graphic state

MAXCOL=7; // Number of colors for edgecolor cycling (plot,plot3,etc.)

cmblack=[1 1 0; // We allocate a minimal colormap as a
  1 0.5 1;     // start for new figures
  0 1 1;
  1 .25 .25;
  .25 1 .25;
  .25 .25 1;
  .75 .75 .75;
  0 0 0;
  1 1 1;
  1 0 0;
  1 0 0;
  1 0 0;
  1 0 0;
  1 0 0;
  1 0 0;
  1 0 0;
  0 0 0;   // background color must me number 17 for correct printing in b/w mode
  1 1 1];  // foreground  ------------------- 18 --------------------------------
           // because of the b/w translation of xset('pattern',n). This is a must
           // if you want axes, titles and axes labels to be really black.

cmwhite=[ 0 0 1;
  0 0.5 0;
  1 0 0;
  0 0.75 0.75;
  .75 0 .75;
  .75 .75 0;
  .25 .25 .25;
  0 0 0;
  1 1 1;
  1 0 1;
  1 0 1;
  1 0 1;
  1 0 1;
  1 0 1;
  1 0 1;
  1 0 1;
  1 1 1; // same remark as blackbg mode.
  0 0 0];
  
if rhs==1 // default background is white
   bg=varargin(1);
else
   bg=[1 1 1];
end


if (type(bg)==1)
      if length(bg)==3
         clum = ([.298936021 .58704307445 .114020904255]*bg(:) >= .5) + 1;
	 if clum==1
	    cmap = cmblack;
	    simpletable=tlist(['simpletable';'y';'m';'c';'b';'g';'r';'w';'k'],1,2,3,6,5,4,9,8);
	  else
	    simpletable=tlist(['simpletable';'y';'m';'c';'b';'g';'r';'k';'w'],6,5,4,1,2,3,8,9);
	    cmap = cmwhite;
	  end
      else
        error('graphinit : background color specification must be a 3-vector')
      end
else
      error('graphinit : background color specification must be a 3-vector')
end 

cmap(17,:)=bg(:)';

defaultRGBcolormap=jetcolormap(64);

clum = ([.298936021 .58704307445 .114020904255]*bg' >= .5) + 1;
if clum==1
     fg = [1 1 1];
     fbg = 0.7*bg + .3*fg;
else
     fg=[0 0 0];
	 fbg=0.7*bg;
end

cmap(16,:)=fbg(:)';

defaultFigureUserData=tlist(['figData';
'defaultViewport';
'background';
'foreground';
'frameColor';
'colormap';
'RGBcolormap';
'RGBcolormaptable';
'simpleColorTable';
'caxisMode';
'caxis';
'margin';
'colorbar';
'typeOfPlot'],...
[0,0,1,1],...
bg,...
fg,...
fbg,...
 cmap,...
 defaultRGBcolormap,...
 [],...
 simpletable,...
 'auto',...
 [],...
 0.12,...
 'off',...
 '');

defaultAxisUserData=tlist(['axisData';'legendHandle';'legendType';...
'colorbarHandle';'colorbarPosition';...
'currentColor';...
'viewport';...
'subplotString';...
'caxisMode';
'caxis';...
'nextPlot'],...
[],0,[],'off',1,[0 0 1 1],'111', 'auto',[],'erase');

ax=gda();
ax.margins=[0 0 0 0];
ax.background=17;
ax.foreground=18;
v=tokens(getversion(),'-');
v(1)='5';
v=sort(v);
if v(1)=='5' // Scilab version > 5
  ax.visible="off";
end
win=gdf();
win.color_map=defaultFigureUserData.colormap;
win.background=16;


%figData_i_h=generic_i_h;
%axisData_i_h=generic_i_h;



// end of graphinit



endfunction
