function vect=axis(varargin)
  
cmd=[];
i=1;
ax=get('current_axes');
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
      ax=varargin(i).handle;
     else
      _error('axis : handle should be an axes handle')
     end
     i=i+1;
   else
     out=parseAxis('axis',varargin(i),'axis','')
     cmd=varargin(i);
     i=i+1;
     break;
   end 
end

if i<= length(varargin)
   _error('axis : too many input arguments')
end

if ~exists('out','local')
  vect=ax.data_bounds;
  vect=vect(:);
  return;
end

if cmd=='tight'
  XLim=_pretty_lims(ax.user_data.data_bounds(1:2),%t);
  YLim=_pretty_lims(ax.user_data.data_bounds(3:4),%t);
  ZLim=_pretty_lims(ax.user_data.data_bounds(5:6),%t);
  out=list('XLim',XLim,'YLim',YLim,'ZLim',ZLim);
end

_update_axes(ax,out);

vect=[];

endfunction
