function out=parseColorBar(typeOfPlot,value,pptystring,ppty)

select type(value)
case 10
  if convstr(value)=='off'
         v='off';
  elseif or(convstr(value)==["left";"westoutside"]) 
         v='left';
  elseif or(convstr(value)==["on";"right";"eastoutside"]) 
         v='right';
  elseif or(convstr(value)==["right";"eastoutside"]) 
         v='right';
  elseif or(convstr(value)==["bot";"bottom";"southoutside"]) 
         v='bottom';
  elseif or(convstr(value)==["top";"norththoutside"]) 
         v='top';
  else
         _error(sprintf('%s : %s is an unknown colorbar spec',typeOfPlot,value));
  end
else
   _error(sprintf('%s : missing colorbar spec',typeOfPlot));
end

out=list(ppty,v);

endfunction
