function out=parseGrid(typeOfPlot,value,pptystring,ppty)

if pptystring=='grid'
      if value=="on"
         out=list('XGrid','on','YGrid','on','ZGrid','on');
      elseif value=='off'
         out=list('XGrid','off','YGrid','of','ZGrid','off');
      elseif type(value)==1
         if length(value)==3
         out=list('XGridColor',value,'YGridColor',value,'ZGridColor',value);
         else
           _error(sprintf('%s : grid color must be a 3-vector',typeOfPlot));
         end
      end
else
  select type(value)
  case 10 //  a string
    select value
      case 'on'
         out=list(ppty,'on');
      case 'off'
         out=list(ppty,'off');
      else
         _error(sprintf('%s : unknown grid spec %s',typeOfPlot,value));
      end
  case 1 // a matrix (must be a nx3 element vector)
     if length(value)==3
         out=list(ppty+"Color",value);
     else
         _error(sprintf('%s : grid color must be a 3-vector',typeOfPlot));
     end
  else
     _error(sprintf('%s : missing grid spec',typeOfPlot));
  end
end


endfunction
