function hidden(varargin) 

[lhs,rhs]=argn(0);

win=get('current_figure');
ax=get('current_axes');

if rhs==1  
  if varargin(1)=='on'
    cmd="on";
  elseif varargin(1)=='off'
    cmd="off";
  else
    _error('hold : unknown hidden state (must be ''on'' or ''off'')')
  end
elseif rhs==0
  cmd="toggle";
end

IMD=win.immediate_drawing;
win.immediate_drawing="off";

h=get(ax,'children');

for i=1:size(h,1);
  _done=%f;
  if or(h(i).user_data.typeOfPlot==["mesh";"trimesh"])
     if cmd=="on" | cmd=="toggle"
         if h(i).user_data.FaceColor=="none"
            h(i).user_data.FaceColor="default";
            h(i).color_mode=findColorIndex(win,ax.user_data.Color);
            _done=%t;
         end
     end  
     if cmd=="off" | (cmd=="toggle" & ~_done)
         if h(i).user_data.FaceColor=="default"
            h(i).user_data.FaceColor="none";
            h(i).color_mode=0;
         end
     end
  end
end

win.immediate_drawing=IMD;



endfunction
