function hdl=quiver3(varargin)

[lhs,rhs]=argn(0);

if ~rhs
  [X,Y] = meshgrid(-2:0.25:2,-1:0.2:1);
  Z = X.* exp(-X.^2 - Y.^2);
  [U,V,W] = surfnorm(X,Y,Z);
  h=gcf();  
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';  
  surf(X,Y,Z);
  hold on
  hdl=quiver3(X,Y,Z,U,V,W,0.5);
  hold off
  axis equal
  legend 'Normals' 
  h.immediate_drawing=IMD;  
return
end

hdl=_mainPlot('quiver3',varargin);

// end of quiver
endfunction
