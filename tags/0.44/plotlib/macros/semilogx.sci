function hdl=semilogx(varargin)

[lhs,rhs]=argn(0);

if ~rhs
   w=logspace(-2,2,200);
   s=%i*w;
   g=1../(s.^2+0.01*s+1);
   hdl=semilogx(w,abs(g));
   return
end


hdl=_mainPlot('semilogx',varargin);

// end of semilogx
endfunction
