function c=colormap(varargin)

c=[];
i=1;

while i<=length(varargin)
   if typeof(varargin(i))=="handle"
     if varargin(i).type=="Axes"
      ax=varargin(i);
      win=ax.parent;
     elseif varargin(i).type=="Figure"
      win=varargin(i);
     else
      _error("axis : handle should be an axes of figure handle")
     end
     i=i+1;
   elseif or(type(varargin(i))==[1,10])
     if ~exists('win','local') 
      win=get('current_figure');
     end
     cmap=parseColormap('colormap',varargin(i),'colormap','ColorMap');
     old_nc=size(win.user_data.Colormap,1);
     win.user_data.Colormaptable=[];
     win.user_data.Colormap=[];
     win.user_data.Colormap=cmap(2);
     IMD=win.immediate_drawing;
     win.immediate_drawing="off";
     activateRGBColormap(win);
     if old_nc~=size(cmap(2),1)
         _update_shaded_plots(win);
     end
     win.immediate_drawing=IMD;
//     c=cmap(2);
     i=i+1;
     break;
   end 
end

if i<= length(varargin)
   _error("colormap : too many input arguments")
elseif length(varargin)==0
   if ~exists('win','local') 
      win=get('current_figure');
   end
   c=win.user_data.Colormap;
end

endfunction
