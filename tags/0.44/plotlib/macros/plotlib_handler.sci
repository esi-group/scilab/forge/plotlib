function plotlib_handler(_win,_x,_y,_ibut)
  
  global SCI5 eventHandlers

  _flag=%f;
  if _ibut~=-1000 // delete event...
    
    _h=scf(_win);

    if SCI5
      _h.event_handler_enable='off';
    else
      seteventhandler('');
    end
    
    if typeof(_h.user_data)~='figureData' // Omar m'a tuer ...
      _h.event_handler_enable='off';
      _h.event_handler='';
      return;
    end 
          
    for _handler=_h.user_data.eventHandlers
        evstr(_handler+'(_win,_x,_y,_ibut,_flag)');
    end

    if SCI5
      _h.event_handler_enable='on';
    else
      seteventhandler('plotlib_handler');
    end
    
  else
        
    if ~execstr('eventHandlers(_win+1)','errcatch')
      for _handler=eventHandlers(_win+1)
        [_flag,_err]=evstr(_handler+'(_win,_x,_y,_ibut,_flag)');
        if _err
          error('Plotlib: error in handler function '+_handler+' when processing event '+string(_ibut));
        end
      end
    end
    
  end

endfunction
