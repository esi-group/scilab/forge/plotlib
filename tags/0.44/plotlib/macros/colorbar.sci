function colorbar(varargin)   

cbpos='right';
i=1;
win=get('current_figure');
ax=get('current_axes');
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
      ax=varargin(i).handle;
     else
      _error("colorbar : handle should be an axes handle")
     end
     i=i+1;
   elseif type(varargin(i))==10
     out=parseColorBar("colorbar",varargin(i),"colorbar","colorbarPosition")
     cbpos=out(2);
     i=i+1;
     break;
   end 
end

if i<= length(varargin)
   _error("colorbar : too many input arguments")
end


IMD=win.immediate_drawing;
win.immediate_drawing="off";

_update_axes(ax,list('colorbarPosition',cbpos,'OuterPosition',ax.user_data.OuterPosition));

win.immediate_drawing=IMD;


endfunction
