demopath = get_absolute_file_path("builder.sce");

subdemolist=['pcolor'
'tripcolor'
'surf'
'surfl'
'trisurf'
'trisurfl'
'mesh'
'trimesh'
'triplot'
'quiver'
'quiver3'
'plot3'
'fill'
'fill3'
'loglog'
'plotyy'];

for i=1:size(subdemolist,'*')
  cmd=['_figure(0)';
  'drawlater';
  'clf';
  '_'+subdemolist(i);
  'title '+subdemolist(i);
  'drawnow'];
  mputl(cmd,demopath+filesep()+subdemolist(i)+'.sce')
end



