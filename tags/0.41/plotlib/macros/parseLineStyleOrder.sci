function out=parseLineStyleOrder(typeOfPlot,value,pptystring,ppty)

//
// Parsing function for the 'LineStyleOrder' property 
//
select type(value)
case 1 // a matrix (must be a nx3 element vector)
   if size(value,2)==3
       out=list(ppty,value);
   else
       _error(sprintf('%s : %s spec must be a n x 3 matrix',typeOfPlot,ppty));
   end
else
   _error(sprintf('%s : missing %s spec',typeOfPlot,ppty));
end

endfunction
