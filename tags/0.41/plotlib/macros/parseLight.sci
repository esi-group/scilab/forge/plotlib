function out=parseLight(typeOfPlot,value,pptystring,ppty);

if type(value)==1
   if length(value)==2
      az=value(1)*%pi/180;
      el=value(2)*%pi/180;
      v=[cos(az)*sin(el);sin(az)*sin(el);cos(el)];
   elseif length(value)==3
      v=value(:);
   else
      _error(sprintf('%s : light specification must be a 2 or 3-vector',typeOfPlot))
   end
else
   _error(sprintf('%s : light specification must be a vector',typeOfPlot))
end

out=list(ppty,v);

endfunction // end of parseLight
