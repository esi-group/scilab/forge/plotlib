function hdl=_quiver(varargin)

[lhs,rhs]=argn(0);

if ~rhs
  [X,Y]=meshgrid(linspace(-1,1,15),linspace(-1,1,15));
  h=_gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';  
  hdl=_quiver(X,Y,Y,-X,'og','markersize',2);
  _axis([-1 1 -1 1]);
  _legend('(-x,x) field');
  h.immediate_drawing=IMD;
  return
end

hdl=_mainPlot('quiver',varargin);

// end of quiver
endfunction
