function out=parseBackFaceLighting(typeOfPlot,value,pptystring,ppty)

if type(value)==10
   if or(value==["lit";"unlit";"reverselit"])
     out=list(ppty,value);
   else
     _error(sprintf('%s : unknown BackFaceLighting specification ''%s''',typeOfPlot,value))
   end
else
   _error(sprintf('%s : BackFaceLighting specification must be a string',typeOfPlot))
end

endfunction // end of parseBackFaceLighting
