function h=_getvalidchildren(A)
  h=[]
  for k=1:size(A,'*')
    a=A(k)
    select a.type
//    case "Fac3d" then
//      h=[a;h]
//    case "Segs" then
//      h=[a;h]
    case "Polyline" then
      h=[a;h]
     case 'Axes'
      ax=a.children
      h=[_getvalidchildren(ax($:-1:1));h]
    case 'Compound'
      for k=size(a.children,'*'):-1:1
	h=[_getvalidchildren(a.children(k));h]
      end
    end
  end
endfunction
