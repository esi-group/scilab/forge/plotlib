// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

pathmacros=get_absolute_file_path('buildmacros.sce');
tbx_build_macros(TOOLBOX_NAME,pathmacros);

// Adaptations for Atoms compatibility (loader no longer called
// at workspace level).


%figureDa_i_h=generic_i_h;
%axesData_i_h=generic_i_h;
%leafData_i_h=generic_i_h;
clear uicontrol uimenu
scilab_uicontrol=uicontrol;
scilab_uimenu=uimenu;

vars=['%figureDa_i_h'
'%axesData_i_h'
'%leafData_i_h'
'scilab_uicontrol'
'scilab_uimenu'];

for i=1:size(vars,'*')
  execstr(sprintf('save(pathmacros+filesep()+''%s.bin'',%s)',vars(i),vars(i)));
end

mputl([mgetl(pathmacros+filesep()+'names');vars],pathmacros+filesep()+'names');
clear tbx_build_macros %figureDa_i_h %axesData_i_h %leafData_i_h scilab_uicontrol scilab_uimenu;
