function hdl=_trimesh(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0
   load(PLOTLIB+'dinosaure.dat')
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';
     hdl=_trimesh(nodes,x,y,z,'edgecolor',[0 0 1]);
   _hidden off
   _axis equal
  h.immediate_drawing=IMD;
else
   hdl=_mainPlot('trimesh',varargin);
end

endfunction /////
