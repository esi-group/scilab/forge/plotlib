function out=plotlib_subplot_handler(win,x,y,ibut,in)
  out=in;
  if ibut==10
    h=scf(win);
    ax=h.children;
    h_matching=[];
    numberOfAxes=size(ax,1);
    for i=1:numberOfAxes;
      xl=ax(i).axes_bounds(1)*h.axes_size(1);
      w=ax(i).axes_bounds(3)*h.axes_size(1);
      yu=ax(i).axes_bounds(2)*h.axes_size(2);
      _h=ax(i).axes_bounds(4)*h.axes_size(2);
      if (x-xl >= 0) & (x-xl <=w) & (y-yu >=0) & (y-yu<=_h) & ax(i).visible=="on" & type(ax(i).user_data)~=1
        h_matching=[h_matching;ax(i)];
      end
    end
    if length(h_matching)
      focus(h_matching);
      out=%t;
    end
  end
endfunction



