function out=parseLineStyle(typeOfPlot,value,pptystring,ppty)

//
// Parsing function for the 'LineStyleOrder' or 'LineStyle' property 
//

linestyle=["-","--","-.",".-",":"];

select ppty
case "LineStyleOrder"
  if type(value)==10
    for ch=value(:)'
       if and(ch~=linestyle)
          _error(sprintf("%s : LineStyleOrder must be a string vector with values ""%s"", ""%s"", ""%s"", ""%s"", ""%s"""),typeOfPlot,linestyle);
       end
    end
  else
    _error(sprintf('%s : missing LineStyleOrder value',typeOfPlot));  
  end
case "LineStyle"
  if type(value)==10
    for ch=value(:)'
       if and(ch~=linestyle)
          _error(sprintf("%s : LineStyle must be a string with value ""%s"", ""%s"", ""%s"", ""%s"", ""%s"""),typeOfPlot,linestyle);
       end
    end
  else
    _error(sprintf('%s : missing LineStyle value',typeOfPlot));  
  end
case "LineWidth"
  if type(value)==1
    value=abs(value);
  else
    _error(sprintf('%s : missing LineWidth value',typeOfPlot));  
  end
end

out=list(ppty,value(:)');


endfunction
