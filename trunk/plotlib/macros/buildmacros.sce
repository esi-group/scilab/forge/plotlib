// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

pathmacros=get_absolute_file_path('buildmacros.sce');
tbx_build_macros(TOOLBOX_NAME,pathmacros);

clear tbx_build_macros 
