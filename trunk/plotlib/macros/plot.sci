function hdl=plot(varargin)

[lhs,rhs]=argn(0);

if ~rhs
pause
   IMD=get(gcf(),'immediate_drawing');
   set(gcf(),'immediate_drawing','off');
   clf();
   t=linspace(0,2*%pi,64);
   subplot(2,2,1); 
   h1=plot(t,cos(t));
   title 'plot(t,cos(t))';
   subplot(2,2,2); 
   h2=plot(t,[cos(t);sin(t);sin(t).*cos(t)]);
   title 'plot(t,[cos(t);sin(t);sin(t).*cos(t)])';
   subplot(2,2,3); 
   h3=plot(t,cos(t),t,cos(t),'og');
   title 'plot(t,cos(t),t,cos(t),''og'''
   subplot(2,2,4); 
   h4=plot(cos(t),sin(t));
   title 'plot(cos(t),sin(t)'
   axis equal
   hdl=[h1;h2;h3;h4];
   set(gcf(),'immediate_drawing',IMD);
   return
end

hdl=_mainPlot('plot',varargin);

// end of plot
endfunction  

