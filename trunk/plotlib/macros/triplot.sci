function triplot(varargin)

[lhs,rhs]=argn(0);

if rhs==0   
  load(plotlibpath()+'/tridem.sod')
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';
  triplot(nodes,xy(1,:),xy(2,:),'edgecolor',[0 0 1]);
  axis equal
  h.immediate_drawing=IMD;
else
  _mainPlot('triplot',varargin);
end

endfunction /////
