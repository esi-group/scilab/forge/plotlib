function shg(varargin)
    if length(varargin)==0
      set(get('current_figure'),"visible","on")
    else
      set(varargin(1),"visible","on")
    end
endfunction
