function _update_axes(ax,argList,new)

global %plotlib_global

if argn(2)<3
  new=%f;
end

_win=ax.parent;
IMD=_win.immediate_drawing;
_win.immediate_drawing='off';

h=ax.user_data;

if new
  ax.axes_visible='on';
  ax.box='on';
  ax.filled='on';
  ax.x_label.visible='on';
  ax.y_label.visible='on';
  ax.z_label.visible='on';
  ax.title.visible='on';
  if ax.view=='3d'
    ax.cube_scaling='on';
    angles=[-37.5,30];
    ax.rotation_angles=[90-angles(2) 270+angles(1)];
  end
end

coord=tlist(['coord';'X';'Y';'Z'],1,2,3);

i=1;

while i<=length(argList)

   ppty=argList(i);
   value=argList(i+1);
   
   h(ppty)=value; 
   
   if ppty=='Visible'

      ax.axes_visible=value;
      if value=='off'
        ax.box='off';
        ax.filled='off';
        ax.grid=[-1 -1 -1];
      else
        ax.box=ax.user_data.Box;
        if ax.user_data.Color~='none'
          ax.filled='on';     
        end   
      end
   elseif ppty=='OuterPosition'

      [Position,colorbarPosition]=changeVP(ax,value);

      h.Position=Position;      
      ax.axes_bounds=Position;  
      if colorbarPosition~=[]
        ax.user_data.colorbarHandle.axes_bounds=colorbarPosition;
        ax.user_data.colorbarHandle.visible='on';
      end
      
      if ax.view=='2d' & h.DataAspectRatioMode=='manual' & h.DataAspectRatio==[1 1 1]
         [h.XLim,h.XLimMode,h.YLim,h.YLimMode]=_ratio_one_lims(ax);
         ax.data_bounds(1:4)=[h.XLim(:);h.YLim(:)];
      end
      
    elseif ppty=='Position'
      
      margin=_win.user_data.margin;
      X=value(1);
      Y=value(2);
      W=value(3);
      H=value(4);
      h.OuterPosition=[X-margin Y-margin W+margin H+margin]/(1-margin);      
      
      ax.axes_bounds=value;

    elseif ppty=='Color'

      if value=='none'
        ax.filled='off';
      elseif value=='default'
        ax.filled='on';
      else 
      ax.background=findColorIndex(_win,value);
      end
    elseif or(ppty==['XColor','YColor','ZColor'])
      k=coord(part(ppty,1));         
      if value=='none'
        ax.axes_visible(k)='off';
      else
        ax.axes_visible(k)='on';            
        c=findColorIndex(_win,value);
        ax.foreground=c;
        ax.font_color=c;
      end
    elseif or(ppty==['XDir';'YDir';'ZDir'])

      k=coord(part(ppty,1));
      sci_value='off';
      if value=='reverse'
        sci_value='on';
      end
      ax.axes_reverse(k)=sci_value;
      
    elseif ppty=='Title'

      ax.title.text=value;

   elseif or(ppty==['XScale';'YScale';'ZScale'])

     flags=ax.log_flags;
     k=coord(part(ppty,1));
     if value=='log'
       c='l';
     else // linear
       c='n';
     end
          
    ax.log_flags=part(flags,1:k-1)+c+part(flags,k+1:3)

   elseif or(ppty==['XGrid';'YGrid';'ZGrid'])
 
      k=coord(part(ppty,1));
      if value=='off'
        ax.grid(k)=-1;
      else
        ax.grid(k)=findColorIndex(_win,ax.user_data(ppty+'Color'));
      end

   elseif or(ppty==['XGridColor';'YGridColor';'ZGridColor'])
 
      k=coord(part(ppty,1));
      ax.grid(k)=findColorIndex(_win,value);

   elseif or(ppty==['XTick';'YTick';'ZTick'])

    ax(convstr(part(ppty,1),'l')+'_ticks')=...
        tlist(['ticks';'locations';'labels'],value(:),string(value(:)));  

   elseif or(ppty==['XTickLabel';'YTickLabel';'ZTickLabel'])

      tick=part(ppty,1:5);
      tickslocations=ax(convstr(part(ppty,1),'l')+'_ticks').locations;
      ntick=length(tickslocations);
      ntickl=size(value(:),1);
      if ntick > ntickl
         value(ntickl+1:ntick)=value($);
      else
         value=value(1:ntick);
      end
      ax(convstr(part(ppty,1),'l')+'_ticks')=...
        tlist(['ticks';'locations';'labels'],tickslocations,value(:));  
        
   elseif ppty=='DataAspectRatioMode'
     
     if value=='auto'
       ax.isoview='off';
       if ax.view=='3d'
         ax.cube_scaling='on';
       end            
       if h.XLimMode=='auto'
         h.XLim=ax.user_data.pretty_data_bounds(1:2);      
       end 
       if h.YLimMode=='auto'
          h.YLim=ax.user_data.pretty_data_bounds(3:4);      
       end   
       if h.ZLimMode=='auto'
         h.ZLim=ax.user_data.pretty_data_bounds(5:6);      
       end  
       ax.data_bounds=[h.XLim(:);h.YLim(:);h.ZLim(:)];       
     end       
    
   elseif ppty=='DataAspectRatio'
   
     if argList(i+1)(:)==[1 1 1]' 
       ax.isoview='on';
       ax.tight_limits='on';
       h.DataAspectRatioMode='manual';     
       if ax.view=='3d'
         ax.cube_scaling='off';
       else
         [h.XLim,h.XLimMode,h.YLim,h.YLimMode]=_ratio_one_lims(ax);
         ax.data_bounds(1:4)=[h.XLim(:);h.YLim(:)];
       end       
     end
     
    elseif or(ppty==['XLim';'YLim';'ZLim'])

     h(ppty+'Mode')='manual';
     ax.user_data(ppty+'Mode')='manual';
     ax.user_data(ppty)=value(:)';
     k=coord(part(ppty,1));
     if ax.user_data.DataAspectRatioMode=='auto' | ax.view=='3d'
       ax.data_bounds(:,k)=value(:);
     else
       [h.XLim,h.XLimMode,h.YLim,h.YLimMode]=_ratio_one_lims(ax);
       ax.data_bounds(1:4)=[h.XLim(:);h.YLim(:)];
     end
     ax.tight_limits='on';
      
    elseif or(ppty==['XLimMode';'YLimMode']) | (ppty=='ZLimMode' & ax.view=='3d')
      
      k=coord(part(ppty,1));

      if value=='manual'
        ax.tight_limits='on';
      elseif ax.user_data.DataAspectRatioMode=='auto' | ax.view=='3d'
        db=matrix(ax.user_data.pretty_data_bounds,2,-1);
        ax.data_bounds(:,k)=db(:,k);
        ax.tight_limits='off';
        h(part(ppty,1:4))=ax.data_bounds(:,k)';
      else        
         ax.user_data(ppty)='auto';       
         [h.XLim,h.XLimMode,h.YLim,h.YLimMode]=_ratio_one_lims(ax);
         ax.data_bounds(1:4)=[h.XLim(:);h.YLim(:)];
         ax.tight_limits='on';
      end
      
   elseif ppty=='CLim'

      ax.user_data.CLim=value;
      h.CLimMode='manual';
      ax.user_data.CLimMode='manual';
      _update_shaded_plots(ax);

   elseif ppty=='CLimMode'

      if value=='auto'
         ax.user_data.CLimMode='auto';
         h.CLim=ax.user_data.data_bounds(7:8);        
         ax.user_data.CLim=ax.user_data.data_bounds(7:8);        
         _update_shaded_plots(ax);
      end      
      
     
   elseif or(ppty==['XAxisLocation';'YAxisLocation'])

      ax(convstr(part(ppty,1),'l')+'_location')=value;  
 
   elseif ppty=='Box'

      ax.box=value;

   elseif ppty=='colorbarPosition'

    ax.user_data(ppty)=value;
    
    if value~='off'
      [_type,ierr]=evstr('h.colorbarHandle.type');
      if ierr | _type==[]
        colorbarHandle=newaxes();
        colorbarHandle.user_data.Tag='colorbar';
        ax.user_data.colorbarHandle=colorbarHandle;
        h.colorbarHandle=colorbarHandle;
      else
        delete(h.colorbarHandle.children);    
      end
      processColorBar(ax);
    else
      execstr('delete(h.colorbarHandle)','errcatch');
    end
    
   elseif ppty=='View'   
      
    ax.rotation_angles=[90-value(2) 270+value(1)];
    if value==[0 90]
       ax.cube_scaling='off';
    elseif ax.isoview=='off'
       ax.cube_scaling='on';
    end   
   
   end

   i=i+2;
end

ax.user_data=h;
_win.immediate_drawing=IMD;

endfunction
