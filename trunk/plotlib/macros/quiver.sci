function hdl=quiver(varargin)

[lhs,rhs]=argn(0);

if ~rhs
  [X,Y]=meshgrid(linspace(-1,1,15),linspace(-1,1,15));
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';  
  hdl=quiver(X,Y,Y,-X,'og','markersize',2);
  axis([-1 1 -1 1]);
  legend('(-x,x) field');
  h.immediate_drawing=IMD;
  return
end

hdl=_mainPlot('quiver',varargin);

// end of quiver
endfunction
