function out=parseRatio(typeOfPlot,value,pptystring,ppty)

select type(value)
case 1 // a matrix (must be a 3 element vector)
   if length(value)~=3
      _error(sprintf('%s : %s spec must be a 3 element vector',typeOfPlot,pptystring));
    end
else
   _error(sprintf('%s : missing %s spec',typeOfPlot,ppty));
end

out=list(ppty,value);

endfunction
