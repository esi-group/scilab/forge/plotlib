function  h=cycleColorsLineStyle(hin,n)
    	
h=hin;
ax=h.parent;
win=ax.parent;
currCol=ax.user_data.currentColor;
currLinest=ax.user_data.currentLineStyle;
cmap=ax.user_data.ColorOrder;
linest=ax.user_data.LineStyleOrder;

steplinecol=%f;
stepmarkercol=%f;
steplinest=%f;

if h.MarkerEdgeColor=="default" & h.Marker~="none"
  h.MarkerEdgeColor=cmap(currCol(2),:);
  stepmarkercol=%t;
end

if h.LineStyle~="none"
 if h.LineStyle=="default"
   h.LineStyle=linest(currLinest);
 end
 if h.Color=="default"
   h.Color=cmap(currCol(1),:);
   steplinecol=%t;
 else
   steplinest=%t;   
 end 
end

if steplinecol
  currCol(1)=currCol(1)+1;
  if currCol(1)>size(cmap,1)
    currCol(1)=1;
    steplinest=%t;
  end
end

if stepmarkercol
  currCol(2)=currCol(2)+1;
  if currCol(2)>size(cmap,1)
    currCol(2)=1;
  end
end

if steplinest
  currLinest=currLinest+1;
  if currLinest>size(linest(:),1)
    currLinest=1;
  end
end

ax.user_data.currentColor=currCol;
ax.user_data.currentLineStyle=currLinest;

simplemap=win.user_data.simpleColorTable;

for ppty=["Color", "MarkerEdgeColor","MarkerFaceColor"]
   if or(h(ppty)==simplemap(1))
       h(ppty)=simplemap(h(ppty));
   end
end

endfunction
